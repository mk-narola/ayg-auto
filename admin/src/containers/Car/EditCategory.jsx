import React, {Component} from 'react'
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import NotificationSystem from 'react-notification-system';
import * as adminActions from "../../actions/adminAction";
import CarCategories from '../../views/CarsMaster/EditCategory';
import { Card } from 'components/Card/Card.jsx';
import carApi from "api/carApi";
import {updateCarsCategory} from "../../actions/carCategoryAction";
class EditCategories extends Component {
    constructor(props)
    {
        super()
        this.state={
            data:[]
        }
    }
    componentDidMount() {
        carApi.getCategories_by_id(this.props.match.params.id)
        .then(response=>{
          
            this.setState({data:response});
        })
        
    }

   
    onSubmit = (values) => {
       this.props.updateCarsCategory(values,()=>{
        this.props.history.push("/car-categories");
       })
       
       
    }
    render() {
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                        <CarCategories initialValues={this.state.data} onSubmit={this.onSubmit}/>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps({carsCategory}) {
    return {updateCarsCategory}
}
export default connect(mapStateToProps, {updateCarsCategory})(EditCategories) 
