import React, {Component} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import NotificationSystem from 'react-notification-system';

import Header from 'components/Header/Header';
import Footer from 'components/Footer/Footer';
import Sidebar from 'components/Sidebar/Sidebar';
import Login from '../App/Login'
import {connect} from "react-redux";
import {style} from "variables/Variables.jsx";
import { checkCookie } from "../../helpers/cookie"
import appRoutes from 'routes/app.jsx';

import {bindActionCreators} from "redux";
import {addNotification, resetNotification} from '../../actions/notificationActions';

class App extends Component {
    constructor(props) {
        super(props);
        this.componentDidMount = this
            .componentDidMount
            .bind(this);
        this.state = {
            logged: false
        };
    }
    componentDidMount() {
        this.notificationSystem = this.refs.notificationSystem;
        if (!sessionStorage.jwt) {
            return false;
        }
    }
    componentDidUpdate(e) {
        if (window.innerWidth < 993 && e.history.location.pathname !== e.location.pathname && document.documentElement.className.indexOf('nav-open') !== -1) {
            document
                .documentElement
                .classList
                .toggle('nav-open');
        }
    }
    componentWillReceiveProps(newProps) {
        const {message, level} = newProps.notification;
        if (message && this.notificationSystem) {
            this.notificationSystem.addNotification({title: (
                        <span data-notify="icon" className="pe-7s-speaker"></span>
                    ), message, level, position: "tr", autoDismiss: 15});
            this.props.actions.resetNotification()
        }
    }
    render() {
       const session = this.props.session
       /*  if (!checkCookie('jwt')) {
            this.props.history.push('/login');
        }
        */
       // console.log(checkCookie('jwt'), sessionStorage.jwt);
        if (!sessionStorage.jwt) {
           
            return (
                <div>
                    <NotificationSystem ref="notificationSystem" style={style}/>
                    <Switch>
                        {appRoutes.map((prop, key) => { < Route onEnter = {
                                requireAuth
                            }
                            path = {
                                prop.path
                            }
                            component = {
                                prop.component
                            }
                            key = {
                                key
                            } />
                        })
}
                        <Route path="/login" name="Login" component={Login}/>
                        <Redirect from="/" to="login"/>
                    </Switch>
                </div>
            )
        }
        return (
            <div className="wrapper">
                <NotificationSystem ref="notificationSystem" style={style}/>
                <Sidebar {...this.props}/>
                <div id="main-panel" className="main-panel">
                    <Header {...this.props}/>
                    <Switch>
                        {appRoutes.map((prop, key) => {

                            if (prop.name === "Notifications") 
                                return (
                                    <Route
                                        path={prop.path}
                                        key={key}
                                        render={routeProps => <prop.component {...routeProps} handleClick={this.handleNotificationClick}/>}/>
                                );
                            
                            if (prop.redirect) 
                                return (<Redirect from={prop.path} to={prop.to} key={key}/>);
                            return (<Route
                                onEnter={requireAuth}
                                path={prop.path}
                                component={prop.component}
                                key={key}/>);
                        })
}
                    </Switch>
                    <Footer/>
                </div>
            </div>
        );
    }
}
function requireAuth(nextState, replace) {
    if (!sessionStorage.jwt) {
        replace({
            pathname: '/login',
            state: {
                nextPathname: nextState.location.pathname
            }
        })
    }
}

function mapStateToProps(state) {
    return {notification: state.notification};
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({
            addNotification,
            resetNotification
        }, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
