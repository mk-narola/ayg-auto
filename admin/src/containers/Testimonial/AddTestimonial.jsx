import React, { Component } from 'react'
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import NotificationSystem from 'react-notification-system';
import * as adminActions from "../../actions/adminAction";
import Testimonial from '../../views/Testimonial/AddTestimonial';
import { Card } from 'components/Card/Card.jsx';
import carApi from "api/carApi";
import { postTestimonial } from "../../actions/carCategoryAction";
class AddTestimonial extends Component {

    onSubmit = (values) => {
       
        this.props.postTestimonial(values, () => {
            this.props.history.push('/testimonial');
        });

    }
    render() {
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Testimonial onSubmit={this.onSubmit} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps({ carsCategory }) {
    return { carsCategory }
}
export default connect(mapStateToProps, { postTestimonial })(AddTestimonial) 
