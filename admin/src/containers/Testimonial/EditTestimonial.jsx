import React, { Component } from 'react'
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import NotificationSystem from 'react-notification-system';
import * as adminActions from "../../actions/adminAction";
import Testimonial from '../../views/Testimonial/EditTestimonial';
import { Card } from 'components/Card/Card.jsx';
import carApi from "api/carApi";
import { updateTestimonial } from "../../actions/carCategoryAction";

class EditTestimonial extends Component {
    constructor(props) {
        super()
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        carApi.testimonialById(this.props.match.params.id)
            .then(response => {               
                this.setState({ data: response });
            })

    }


    onSubmit = (values) => {
        
        this.props.updateTestimonial(values, () => {
            this.props.history.push("/testimonial");
        })


    }
    render() {
       
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Testimonial initialValues={this.state.data} onSubmit={this.onSubmit} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps({ updateTestimonial }) {
    return { updateTestimonial }
}
export default connect(mapStateToProps, { updateTestimonial })(EditTestimonial) 
