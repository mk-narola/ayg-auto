import React, { Component } from 'react'
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import NotificationSystem from 'react-notification-system';
import * as adminActions from "../../actions/adminAction";
import Seo from '../../views/SeoMaster/EditSeo';
import { Card } from '../../components/Card/Card.jsx';
import seoApi from "../../api/seoApi";
import { updateSeo } from "../../actions/carCategoryAction";
class EditSeo extends Component {
    constructor(props) {
        super()
        this.state = {
            data: []
            
        }
    }
    componentDidMount() {
        seoApi.getSeo_by_id(this.props.match.params.id)
            .then(response => {
               
                this.setState({ data: response });
            })

    }


    onSubmit = (values) => {     
        this.props.updateSeo(values, this.props.match.params.id, () => {
            this.props.history.push("/seo");
        })
    }
    render() {
       
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Seo initialValues={this.state.data} onSubmit={this.onSubmit} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps({ updateSeo }) {
    return { updateSeo }
}
export default connect(mapStateToProps, { updateSeo })(EditSeo) 
