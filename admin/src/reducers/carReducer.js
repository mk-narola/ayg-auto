import * as types from '../actions/actionTypes'
import intialState from './intialState'

export default function (state = intialState.cars,action) {
    switch (action.type) {
        case types.CAR_ADD_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.CAR_ADD_SUCCESS:
            return {
                ...state,
                isFetching: false,
                addCar: true
            }
        case types.CAR_ADD_FAILER:
            return {
                ...state,
                isFetching: false,
                addCar: false
            }



        case types.CAR_STOCKS_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.CAR_STOCKS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                carsStocks: action.payload
            
            }
        case types.CAR_STOCKS_FAILER:
            return {
                ...state,
                isFetching: false,
                carsStocks: false
            }


        case types.CAR_AVAILABLE_STOCKS_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.CAR_AVAILABLE_STOCKS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                carAvailable: action.payload
            }
        case types.CAR_AVAILABLE_STOCKS_FAILER:
            return {
                ...state,
                isFetching: false,
                carAvailable: false
            }



        case types.CAR_UPDATE_STOCKS_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.CAR_UPDATE_STOCKS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                carUpdate: true
            }
        case types.CAR_UPDATE_STOCKS_FAILER:
            return {
                ...state,
                isFetching: false,
                carUpdate: false
            }    
        default:
            return state
    }
}