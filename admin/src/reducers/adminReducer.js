import * as types from '../actions/actionTypes'
import intialState from './intialState'

export default function (state = intialState.admin, action){
    switch(action.type){
        case types.ADMIN_ACCOUNT_REQUEST:
           return {
               ...state,
               isFetching: true
           }
        case types.ADMIN_ACCOUNT_FAILER:
            return {
                ...state,
                isFetching: false,
                user: false
            }
        case types.ADMIN_ACCOUNT_SUCCESS:
           return {
               ...state,
               isFetching: false,
               user: action.payload
           }   
        default:
           return state
    }
}