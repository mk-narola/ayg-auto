import * as type from '../actions/actionTypes'
import intialState from './intialState'
const LOAD = 'LOAD'

const reducer = (state = {}, action) => {
  switch (action.type) {
    case LOAD:
      return {
        data: action.data
      }

      case type.CAR_CATEGORIES_REQUEST:
      return {
          ...state,
          isFetching: true
      }
    case type.CAR_CATEGORIES_SUCCESS:
      return {
          ...state,
          isFetching: false,
          addCarCategory: true
      }
    case type.CAR_CATEGORIES_FAILER:
      return {
          ...state,
          isFetching: false,
          addCarCategory: false
      }
    default:
      return state
  }
}

/**
 * Simulates data loaded into this reducer from somewhere
 */

export default reducer;