import * as types from './actionTypes';
import sessionApi from "../api/sessionApi";

export function loginSuccess() {
    return { type: types.LOG_IN_SUCCESS }
}

export function loginFailer(payload) {
    return { type: types.LOG_IN_FAILER, payload }
}

export function loginRequest() {
    return { type: types.LOG_IN_REQUEST }
}

export function loginUser(credentials) {
    return function (dispatch) {
        dispatch(loginRequest)
        return sessionApi.login(credentials).then(response => {
            if(response.status === 'ok'){
                sessionStorage.setItem('jwt', response.token);
                dispatch(loginSuccess())     
            }               
            if(response.status === 'error'){
                dispatch(loginFailer(response.error))   
            }
        }).catch(error => {           
            throw error            
        })
    }
}

export function logOutUser() {
    sessionStorage.removeItem('jwt');
    return { type: types.LOG_OUT }
}