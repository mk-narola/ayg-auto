import * as types from './actionTypes';
import carApi from '../api/carApi'
import seoApi from '../api/seoApi'
import { addNotification } from "./notificationActions";

export function addCarCategoryRequestInit(){
    return { type: types.CAR_CATEGORIES_REQUEST }
}

export function addCarCategoryRequestSuccess(payload){
    return {type: types.CAR_CATEGORIES_SUCCESS, payload}
}

export function addCarCategoryRequestFail(){
    return { type: types.CAR_CATEGORIES_FAILER}
}

export function postCarsCategory(request,callback) {
    return function(dispatch) {
        dispatch(addCarCategoryRequestInit())
        return carApi.addCategory(request).then(response => {
            if(response.status === 'success'){                
                dispatch(addCarCategoryRequestSuccess());
                dispatch(addNotification(response.message,'success'));
                callback();
            }
            if(response.error){
                dispatch(addCarCategoryRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}




export function deleteCarsCategory(id,callback) {
    return function(dispatch) {
        dispatch(addCarCategoryRequestInit())
        return carApi.deleteCategories(id).then(response => {
            if(response.status === 'success'){                
                dispatch(addCarCategoryRequestSuccess());
                dispatch(addNotification(response.message,'success'));
                callback();
            }
            if(response.error){
                dispatch(addCarCategoryRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}

export function updateCarsCategory(request,callback) {
    return function(dispatch) {
        dispatch(addCarCategoryRequestInit())
        return carApi.editCategory(request).then(response => {
            if(response.status === 'success'){                
                dispatch(addCarCategoryRequestSuccess());
                dispatch(addNotification(response.message,'success'));
                callback();
            }
            if(response.error){
                dispatch(addCarCategoryRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}

export function deleteTestimonial(id, callback) {
    return function (dispatch) {
        dispatch(addCarCategoryRequestInit())
        return carApi.deleteTestimonial(id).then(response => {
            if (response.status === 'success') {
                dispatch(addCarCategoryRequestSuccess());
                dispatch(addNotification(response.message, 'success'));
                callback();
            }
            if (response.error) {
                dispatch(addCarCategoryRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}

export function postTestimonial(request, callback) {
    return function (dispatch) {
        dispatch(addCarCategoryRequestInit())
        return carApi.addTestimonial(request).then(response => {
            if (response.status === 'success') {
                dispatch(addCarCategoryRequestSuccess());
                dispatch(addNotification(response.message, 'success'));
                callback();
            }
            if (response.error) {
                dispatch(addCarCategoryRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}

export function updateTestimonial(request, callback) {
    return function (dispatch) {
        dispatch(addCarCategoryRequestInit())
        return carApi.editTestimonial(request).then(response => {
            if (response.status === 'success') {
                dispatch(addCarCategoryRequestSuccess());
                dispatch(addNotification(response.message, 'success'));
                callback();
            }
            if (response.error) {
                dispatch(addCarCategoryRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}

export function updateSeo(request,id, callback) {
    return function (dispatch) {
        dispatch(addCarCategoryRequestInit())
        return seoApi.editSeo(request,id).then(response => {
            if (response.status === 'success') {
                dispatch(addCarCategoryRequestSuccess());
                dispatch(addNotification(response.message, 'success'));
                callback();
            }
            if (response.error) {
                dispatch(addCarCategoryRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}
