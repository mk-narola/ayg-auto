import * as types from './actionTypes';
import carApi from '../api/carApi'
import { addNotification } from "./notificationActions";

export function addCarRequestInit(){
    return { type: types.CAR_ADD_REQUEST }
}

export function addCarRequestSuccess(payload){
    return {type: types.CAR_ADD_SUCCESS, payload}
}

export function addCarRequestFail(){
    return { type: types.CAR_ADD_FAILER }
}

export function postCars(request,callback) {
    return function(dispatch) {
        dispatch(addCarRequestInit())
        return carApi.addCar(request).then(response => {
            if(response.status === 'success'){                
                dispatch(addCarRequestSuccess());
                dispatch(addNotification(response.message,'success'));
                callback();
            }
            if(response.error){
                dispatch(addCarRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}

export function updateCars(request,callback) {
    return function(dispatch) {
        dispatch(addCarRequestInit())
        return carApi.editCar(request).then(response => {
            if(response.status === 'success'){                
                dispatch(addCarRequestSuccess());
                dispatch(addNotification(response.message,'success'));
                callback();
            }
            if(response.error){
                dispatch(addCarRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}
export function deleteCars(id,callback) {
    return function(dispatch) {
        dispatch(addCarRequestInit())
        return carApi.deleteCars(id).then(response => {
            if(response.status === 'success'){                
                dispatch(addCarRequestSuccess());
                dispatch(addNotification(response.message,'success'));
                callback();
            }
            if(response.error){
                dispatch(addCarRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }
}

export function deleteImg(id,imgid,callback) {
    
    return function(dispatch) {
        dispatch(addCarRequestInit())
        return carApi.deleteImage(id,imgid).then(response => {
            if(response.status === 'success'){                
                dispatch(addCarRequestSuccess());
                dispatch(addNotification(response.message,'success'));
                callback();
            }
            if(response.error){
                dispatch(addCarRequestFail());
                dispatch(addNotification(response.error.message, 'error'));
            }
        }).catch(error => {
            dispatch(addNotification(error.message, 'error'));
        })
    }

}
   


export function carsStocksRequestInit() {
    return { type: types.CAR_STOCKS_REQUEST }
}
export function carsStocksRequestSuccess(payload) {
    return { type: types.CAR_STOCKS_SUCCESS, payload }
}
export function carsStocksRequestFail() {
    return { type: types.CAR_STOCKS_FAILER }
}
export function carsStocks() {
    return function (dispatch) {
        dispatch(carsStocksRequestInit())
        return carApi.getCars().then(response => {
            dispatch(carsStocksRequestSuccess(response));
        })
            .catch(error => {
                throw error
            })
    }
}



export function carsAvailableStocksRequestInit() {
    return { type: types.CAR_AVAILABLE_STOCKS_REQUEST }
}
export function carsAvailableStocksRequestSuccess(payload) {
    return { type: types.CAR_AVAILABLE_STOCKS_SUCCESS, payload }
}
export function carsAvailableStocksRequestFail() {
    return { type: types.CAR_AVAILABLE_STOCKS_FAILER }
}
export function carAvailablesStocks() {
    return function (dispatch) {
        dispatch(carsAvailableStocksRequestInit());
        return carApi.getStocks().then(response => {
            dispatch(carsAvailableStocksRequestSuccess(response))
        })
            .catch(error => {
                throw error
            })
    }
}




export function carsUpdateStocksRequestInit() {
    return { type: types.CAR_AVAILABLE_STOCKS_REQUEST }
}
export function carsUpdateStocksRequestSuccess(payload) {
    return { type: types.CAR_AVAILABLE_STOCKS_SUCCESS, payload }
}
export function carsUpdateStocksRequestFail() {
    return { type: types.CAR_AVAILABLE_STOCKS_FAILER }
}
export function carUpdateStocks(req, callback) {
    return function (dispatch) {
        dispatch(carsUpdateStocksRequestInit())
        return carApi.updateStocks(req).then(response => {
            dispatch(carsUpdateStocksRequestSuccess(response));
            dispatch(addNotification(response.message, 'success'));
            callback();
           
        })
            .catch(error => {
                throw error
            })
    }
}


