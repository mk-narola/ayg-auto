import React, { Component } from 'react';
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';
import { Field, reduxForm, initialize } from "redux-form";

import Loader from "../../components/Loader/Loader";
import { Card } from 'components/Card/Card.jsx';
import Button from 'elements/CustomButton/CustomButton.jsx';

const required = value => value
    ? undefined
    : 'This Field Is Required.'
const confirmpassword = value => document.getElementById("password").value  === value ? undefined : 'Passwords Do Not Match!' 
const renderField = ({ id ,input, label, type, meta: { touched, error, warning },disabled }) => {
    const errorClass = ((touched) && error) ? 'form-group has-error' : 'form-group';
    return (

        <div className={errorClass}>
            <label className="control-label">{label} <span className="required">*</span></label>
            <input id={id} {...input} placeholder={label} type={type} className="form-control" disabled={disabled}/>
            {touched && ((error && <p className="help-block">{error}</p>) || (warning && <p className="help-block">{warning}</p>))}
        </div>
    )
}

let UserProfile = (props) => {
    const { handleSubmit, pristine, reset, submitting } = props
    const userdata = props.userdata
    return (
        <Grid fluid>         
            <Row>
                <Col md={8} mdOffset={2}>
                    <Card
                        title="Edit Profile"
                        content={
                            <form method="post" onSubmit={handleSubmit(props.onSubmit)}>
                                <Row>
                                    <div className="col-md-6">
                                        <Field
                                            id="name"
                                            name="name"                                           
                                            label="Name"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <Field
                                            id="email"
                                            name="email"
                                            type="email"
                                            label="Email Address"
                                            component={renderField}
                                            disabled = 'true'
                                            validate={[required]}
                                        />
                                    </div>
                                </Row>
                                <Row>
                                    <div className="col-md-6">
                                        <Field
                                            id="password"
                                            name="password"
                                            label="New Password"
                                            type="password"
                                            component={renderField}
                                            validate={[required]}
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <Field
                                            id="password_repeat"
                                            name="password_repeat"
                                            label="Repeat Password"
                                            type="password"
                                            component={renderField}
                                            validate={[confirmpassword, required]}
                                        />                                        
                                    </div>
                                </Row>
                                <button type="submit" className="btn-fill pull-right btn btn-info">Update Profile</button>
                                <div className="clearfix"></div>
                            </form>
                        }
                    />
                </Col>
            </Row>
        </Grid>
    )
}
UserProfile = reduxForm({
    form: 'userform',
    enableReinitialize: true
})(UserProfile)

export default UserProfile;