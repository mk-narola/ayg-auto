import React, { Component } from 'react';
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';
import { Field, reduxForm, initialize,FieldArray } from "redux-form";
import Radio from '../../elements/CustomRadio/CustomRadio';
import Loader from "../../components/Loader/Loader";
import { Card } from '../../components/Card/Card.jsx';
import DropDownSelect from "../../components/Basic/DropDownSelect";
import Button from '../../elements/CustomButton/CustomButton.jsx';
import _ from 'lodash';
import Ckeditor from "../../components/CKEditor/Ckeditor";
import TextboxD from "../../components/Dynamic/TextboxD";
const imagePath = 'http://aygauto.com/backend/public/cars_images/';
const required = value => value
    ? undefined
    : 'This Field Is Required.'
const renderField = ({id ,input, label, type, meta: { touched, error, warning },disabled }) => {
    
    const errorClass = ((touched) && error) ? 'form-group has-error' : 'form-group';
    return (
        <div className={errorClass}>
            <label className="control-label">{label} <span className="required"></span></label>
            <input id={id} {...input} placeholder={label} type={type} className="form-control" disabled={disabled}/>
            {touched && ((error && <p className="help-block">{error}</p>) || (warning && <p className="help-block">{warning}</p>))}
        </div>
    )
}
const renderFile = ({id,input,label,type}) => {
    let newInput = _.omit(input, ['value'])
return (
    <div>
     <label className="control-label"> {label} </label>   
     <input id={id} {...newInput} type={type}  className="form-control"/>
     </div>
    )
}

const multipleFile = ({id,input,label,type}) => {
    let newInput = _.omit(input, ['value'])
    return (
        <div className="form-group">
        <label className="control-label"> {label} </label>  
        <input id={id} {...newInput} type={type}  className="form-control" multiple/>
        </div>    
    )
}

const renderTextArea = ({ id, input, label, type, meta: { touched, error, warning }, disabled }) => {
    const errorClass = ((touched) && error) ? 'form-group has-error' : 'form-group';
    return (
        <div className={errorClass}>
            <label className="control-label">{label} </label>
            <textarea id={id} {...input} placeholder={label} type={type} className="form-control" rows="3" cols="40" disabled={disabled}  >

            </textarea>
            {touched && ((error && <p className="help-block">{error}</p>) || (warning && <p className="help-block">{warning}</p>))}
        </div>


    )
}

const renderEditor = ({input, rest}) => {
  
   return (
       <div {...input}>
           <Ckeditor input={input}/>
        </div>   
   )
   
}

const categories = ['Economical','Sedan','Coupe','Hatchback','Convertible','Wagon','SUV','Mini-Van','Truck']
const car_type = ['new-cars', 'used-cars', 'new-and-used-cars']
const car_color = ['White','Black','Grey','Grey-light','Red','Blue','Orange']
const car_transmission = ['Automatic','Manual']


const renderMembers = ({ fields,title}) =>{

    return ( 
        <div>
        {fields.map((field,index) => (
                    <Field
                    id={`${field}`}
                    name={`${field}`}                                           
                    label={`${title}`}
                    type="text"  
                    key={`${index}`}                  
                    component={renderField}
                    disabled
                    />
        ))}
        {/*<button type="button" onClick={this.add} className="btn btn-primary mb-20" onClick={() => fields.push("")}>{`Add.${title}`}</button>*/}
        </div>
    )
}
//slider_images:"["055103BMW-5-Series-Right-Front-Three-Quarter-101056.gif","055103images.jpg","055103list-img.jpg","055103media_1087_750_s_c1.jpg"]"
let ViewCar = (props) => {
    //console.log('Property here',props);
   
    const { handleSubmit, pristine, reset, submitting,initialValues } = props      
   
    return (
        <Grid fluid>         
            <Row>               
                <Col md={12}>
                    <Card
                        title="DisplayCars"
                        content={
                            <form id="addCar" method="post" encType="multipart/form-data" >
                            <Row>
                                <Col md={4}>
                                <div className="form-group">
                                
                                <Field
                               
                                id="name"
                                name="car_categories"                                           
                                label="Body Style"
                                type="text"
                                component={renderField}
                                validate={[required]}
                                disabled
                                >
                                </Field>
                                </div>
                                </Col>
                                <Col md={4}>
                                        <Field
                                            id="name"
                                            name="brand"                                           
                                            label="brand"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                      
                                   </Col>
                                    <Col md={4}>
                                        <Field
                                            id="name"
                                            name="model_name"                                           
                                            label="Model Name"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                   </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>
                                        <Field
                                            id="name"
                                            name="price"                                           
                                            label="Price"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                   </Col>
                                    <Col md={4}>
                                        <Field
                                            id="name"
                                            name="year"                                           
                                            label="Year"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                   </Col>
                                    <Col md={4}>
                                        <Field
                                            id="name"
                                            name="miles"                                           
                                            label="Miles"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                   </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>
                                        <div className="form-group">
                                           
                                                <Field                                               
                                                id="name"
                                                name="car_color"                                           
                                                label="Color Of Car"
                                                type="text"
                                                component={renderField}
                                                validate={[required]}
                                                disabled
                                                >
                                                </Field>
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                    <div className="form-group">
                                        <label htmlFor="car_transmission" className="control-label">transmission Of Car</label>
                                            <Field
                                                name="car_transmission"
                                                label="transmission Of Car"
                                                component={DropDownSelect}
                                                categories={car_transmission}
                                                className="form-control"
                                                disabled
                                                >
                                        </Field>
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                    
                                        <br/>
                                       
                                       <img height="100px" width="150px" src={`${imagePath}${props.initialValues.thumbnail}`}/> 
                                    </Col>
                                </Row>
                                    <h4 className="title ml-15"><b> Basic Information </b></h4>
                                    <Row> 
                                    <Col md={3}> 
                                    <Field
                                            id="Passenger_Capacity"
                                            name="passenger_capacity"                                           
                                            label="Passenger Capacity"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="Wheel"
                                            name="wheel"                                           
                                            label="wheel"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="tire"
                                            name="tire"                                           
                                            label="Tire"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="PowerTrain Warranty"
                                            name="powertrain_warranty"                                           
                                            label="PowerTrain Warranty"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                    </Col>
                                    </Row>
                                    <Row> 
                                    <Col md={3}> 
                                    <Field
                                            id="Engine"
                                            name="engine"                                           
                                            label="Engine"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="Horsepower"
                                            name="horsepower"                                           
                                            label="Horsepower"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="transmission"
                                            name="transmission"                                           
                                            label="Transmission"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="Stock Number"
                                            name="stock_number"                                           
                                            label="Stock Number"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                        />
                                    </Col>
                                    </Row>
                                    <Row> 
                                    <Col md={3}>                                     
                                    <Field
                                            id="VIN"
                                            name="vin"                                           
                                            label="VIN"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                    />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="MPG_CITY"
                                            name="mpg_city"                                           
                                            label="MPG CITY"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                    />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="MPG_HWY"
                                            name="mpg_hwy"                                           
                                            label="MPG HWY"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                    />
                                    </Col>
                                    <Col md={3}>
                                    <Field
                                            id="Drivetrain"
                                            name="drivetrain"                                           
                                            label="Drivetrain"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                            disabled
                                    />
                                    </Col> 
                                </Row>
                                <h4 className="title ml-15"> <b>Other Information</b> </h4>
                                <Row>
                                    <Col md={4}>                    
                                    <FieldArray title="ACCESSORY PACKAGES"  name="accessory_packages" component={renderMembers}  />                                                                
                                    </Col>
                                    <Col md={4}>                                                                 
                                    <FieldArray title="BRAKING & TRACTION"  name="braking_traction" component={renderMembers} />                                                                 
                                    </Col>
                                    <Col md={4}>                                                                   
                                    <FieldArray title="COMFORT & CONVENIENCE" name="comfort_convenience" component={renderMembers}  />                                                                
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>                                                                  
                                    <FieldArray title="ENTERTAINMENT & INSTRUMENTATION" name="entertainment_instrumentation" component={renderMembers}  />                                                               
                                    </Col>
                                    <Col md={4}>                                                                 
                                    <FieldArray title="LIGHTING" name="lighting" component={renderMembers} />                                                                
                                    </Col>
                                    <Col md={4}>                                                                  
                                    <FieldArray title="EXTERIOR" name="exterior" component={renderMembers}  />                                                               
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>                                                                  
                                    <FieldArray title="SEATS" name="seats" component={renderMembers}/>                                                             
                                    </Col>
                                    <Col md={4}>                                                                   
                                    <FieldArray title="SAFETY & SECURITY" name="safety_security" component={renderMembers}  />                                                                 
                                    </Col>
                                    <Col md={4}>                                                                 
                                    <FieldArray title="WHEELS & TIRES" name="wheels_tires" component={renderMembers}  />                                                                
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>                                    
                                    <FieldArray title="STEERING" name="steering" component={renderMembers}  />                                                                  
                                    </Col>
                                </Row>

                                <h4 className="title ml-15"> Seo For Current Cars </h4>
                                <Row>
                                    <Col md={4}>
                                        <Field
                                            id="meta_title"
                                            name="meta_title"
                                            label="Meta Title"
                                            type="textarea"
                                            component={renderTextArea}
                                            disabled

                                        />
                                    </Col>
                                    <Col md={4}>
                                        <Field
                                            id="meta_description"
                                            name="meta_description"
                                            label="Meta Description "
                                            type="textarea"
                                            isnotrequired="true"
                                            component={renderTextArea}
                                            disabled
                                        />
                                    </Col>
                                    <Col md={4}>
                                        <Field
                                            id="keyword"
                                            name="keyword"
                                            label="Keyword "
                                            type="textarea"
                                            component={renderTextArea}
                                            disabled


                                        />

                                    </Col>
                                </Row>
                                <h4 className="title ml-15"> <b>Images And Video </b></h4>
                                <Row>
                                    <Col md={8}>
                                   
                                         <br/>
                                        
                                    { props.initialValues.slider_images && props.initialValues.slider_images.map(function(item, index)
                                    {        //                    
                                        
                                      return(  <button type="button" className="btn btn-primary mb-20">
                                      <img id={index} key={item} height="100px" width="100px" src={`${imagePath}${item}`}/></button>);
                                    })}
                                    </Col>
                                    <Col md={2}>
                                        <div className="form-group">
                                            <label htmlFor="car_type" className="control-label">Select Car Type</label>
                                            <Field
                                                name="car_type"
                                                // component="select"
                                                label="car_type"
                                                component={DropDownSelect}
                                                categories={car_type}
                                                className="form-control"
                                                disabled
                                            >
                                            </Field>
                                        </div>
                                    </Col>
                                    <Col md={2}>
                                     <FieldArray title="Youtube" name="video_urls" component={renderMembers} disabled />
                                    </Col>
                                </Row>    
                                
                            </form>
                        }
                    />
                </Col>
            </Row>
        </Grid>
    )
}
ViewCar = reduxForm({
    form: 'carForm',
    enableReinitialize: true
})(ViewCar)

export default ViewCar;
