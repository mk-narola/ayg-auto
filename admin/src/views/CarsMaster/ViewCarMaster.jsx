import React, { Component } from 'react';

import {
    Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel,
    FormControl
} from 'react-bootstrap';
import Modal from "react-responsive-modal";
import _ from "lodash";
import {connect} from "react-redux"
import {bindActionCreators} from "redux";
import {Card} from '../../components/Card/Card.jsx';

import Cars from "./ViewCar";
const styles = {
  fontFamily: "sans-serif",
  textAlign: "center",
 
 
};
 
class ViewCarMaster extends Component {
  
    render() {
        const { open } = this.props;
       const{data}=this.props.viewdata
      
        return (       
                        <div style={styles}>
                            <Modal   open={open} onClose={()=>this.props.onCloseModal()} >
                            <div><h2>Display Car Information</h2></div>
                                   
                            <div className="content" >
                            <Grid fluid>
                                <Row>
                                    <Col md={12}>
                                        <Cars initialValues={this.props.viewdata}/>
                                    </Col>
                                </Row>
                            </Grid>
                             </div>
                             
                            </Modal>
                        </div>
                            
           
        );
    }
}

export default ViewCarMaster;