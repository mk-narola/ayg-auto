import React, { Component } from 'react';

import {
    Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel,
    FormControl
} from 'react-bootstrap';
import Modal from "react-responsive-modal";
import _ from "lodash";

import {connect} from "react-redux"
import {bindActionCreators} from "redux";
import {Card} from '../../components/Card/Card.jsx';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import Cars from "./ViewCar";
const styles = {
  fontFamily: "sans-serif",
  textAlign: "center",
 
 
};
 
class DemoView extends Component {
    constructor(props, context) {
        super(props, context);
    
        this.handleSelect = this.handleSelect.bind(this);
    
        this.state = {
          key: 1
        };
      }
    handleSelect(key) {
        alert(`selected ${key}`);
        this.setState({ key });
      }
    render() {
        const { open } = this.props;
        const{data}=this.props.viewdata
      
        return (       
                        <div style={styles}>
                            <Modal open={open} onClose={()=>this.props.onCloseModal()} little>
                           
                            <Tabs>
                                <TabList>
                                <Tab>Car Category</Tab>
                                <Tab >Basic Information</Tab>
                                <Tab>Other Information</Tab>
                                <Tab>Image&Video</Tab>
                                <Tab disabled>Toad</Tab>
                                </TabList>

                                <TabPanel>
                                <p>
                                    <b>Car Category</b> 
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                                
                                        </tbody>
                                    </table>

                                </p>
                               
                                </TabPanel>
                                <TabPanel>
                                <p>
                                    <b>Luigi</b>
                                          
                                    <Row>               
                                        <Col md={12}>
                                            <Card
                                                title="DisplayCars"
                                                content={ <form>
                                                    <Row>
                                                    <Col md={4}>
                                                    
                                                    <label  className="control-label">Body  Style</label>
                                                    <br/>
                                                    <label  className="control-label"> Style</label>
                                                   
                                                    </Col>
                                                    <Col md={4}>
                                                    
                                                    <label  className="control-label">Body  Style</label>
                                                    <br/>
                                                    <label  className="control-label"> Style</label>
                                                   
                                                    </Col>
                                                    <Col md={4}>
                                                    
                                                    <label  className="control-label">Body  Style</label>
                                                    <br/>
                                                    <label  className="control-label"> Style</label>
                                                   
                                                    </Col>
                                                    </Row></form>}
                                                />
                                        </Col>
                                    </Row>
                                   
                                </p>
                                
                                </TabPanel>
                                <TabPanel>
                                <p>
                                    <b>Princess Peach</b> (<i>Japanese: ピーチ姫 Hepburn: Pīchi-hime, [piː.tɕi̥ çi̥.me]</i>)
                                    is a character in Nintendo's Mario franchise. Originally created by Shigeru Miyamoto,
                                    Peach is the princess of the fictional Mushroom Kingdom, which is constantly under
                                    attack by Bowser. She often plays the damsel in distress role within the series and
                                    is the lead female. She is often portrayed as Mario's love interest and has appeared
                                    in Super Princess Peach, where she is the main playable character.
                                </p>
                               
                                </TabPanel>
                                <TabPanel>
                                <p>
                                    <b>Yoshi</b> (<i>ヨッシー Yosshī, [joɕ.ɕiː]</i>) (<i>English: /ˈjoʊʃi/ or /ˈjɒʃi/</i>), once
                                    romanized as Yossy, is a fictional anthropomorphic dinosaur who appears in
                                    video games published by Nintendo. Yoshi debuted in Super Mario World (1990) on the
                                    Super Nintendo Entertainment System as Mario and Luigi's sidekick. Yoshi later starred
                                    in platform and puzzle games, including Super Mario World 2: Yoshi's Island, Yoshi's Story
                                    and Yoshi's Woolly World. Yoshi also appears in many of the Mario spin-off games, including
                                    Mario Party and Mario Kart, various Mario sports games, and Nintendo's crossover fighting
                                    game series Super Smash Bros. Yoshi belongs to the species of the same name, which is
                                    characterized by their variety of colors.
                                </p>
                               
                                </TabPanel>
                                <TabPanel>
                                <p>
                                    <b>Toad</b> (<i>Japanese: キノピオ Hepburn: Kinopio</i>) is a fictional character who primarily
                                    appears in Nintendo's Mario franchise. Created by Japanese video game designer Shigeru Miyamoto,
                                    he is portrayed as a citizen of the Mushroom Kingdom and is one of Princess Peach's most loyal
                                    attendants; constantly working on her behalf. He is usually seen as a non-player character (NPC)
                                    who provides assistance to Mario and his friends in most games, but there are times when Toad(s)
                                    takes center stage and appears as a protagonist, as seen in Super Mario Bros. 2, Wario's Woods,
                                    Super Mario 3D World, and Captain Toad: Treasure Tracker.
                                </p>
                                
                                </TabPanel>
                            </Tabs>

                            </Modal>
                        </div>
                            
           
        );
    }
}

export default DemoView;