import React, { Component } from 'react';
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';
import { Field, reduxForm, initialize } from "redux-form";

//import { connect } from 'react-redux'
//import { load as loadAccount } from '../../reducers/';

import Loader from "../../components/Loader/Loader";
import { Card } from '../../components/Card/Card.jsx';
import Button from '../../elements/CustomButton/CustomButton.jsx';
import _ from 'lodash';
const imagePath = 'http://aygauto.com/backend/public/ayg_images/';
const required = value => value
    ? undefined
    : 'This Field Is Required.'
const renderField = ({ id, input, label, type, meta: { touched, error, warning }, disabled }) => {
    const errorClass = ((touched) && error) ? 'form-group has-error' : 'form-group';
    return (
        <div className={errorClass}>
            <label className="control-label">{label} </label>
            <input id={id} {...input} placeholder={label} type={type}  className="form-control" disabled={disabled} />
            {touched && ((error && <p className="help-block">{error}</p>) || (warning && <p className="help-block">{warning}</p>))}
        </div>
    )
}
const renderTextArea = ({ id, input, label, type, meta: { touched, error, warning }, disabled }) => {
    const errorClass = ((touched) && error) ? 'form-group has-error' : 'form-group';
    return (
        <div className={errorClass}>
            <label className="control-label">{label} </label>
            <textarea id={id} {...input} placeholder={label} type={type} className="form-control" rows="3" cols="125"  >

            </textarea>
            {touched && ((error && <p className="help-block">{error}</p>) || (warning && <p className="help-block">{warning}</p>))}
        </div>

        
    )
}

let EditSeo = (props) => {
   
    const { handleSubmit, pristine, reset, submitting } = props
   
    return (
        <Grid fluid>
            <Row>
                <Col md={12}>
                    <Card
                        title="Edit Seo"

                        content={
                            <form id="editSeoForm" onSubmit={handleSubmit(props.onSubmit)} method="post" encType="multipart/form-data" >
                                                           
                                <Row>
                                    <div className="col-md-10">
                                        <Field
                                            id="route"
                                            name="route"
                                            label="Rout"
                                            type="text"
                                            component={renderField}
                                            disabled
                                            validate={[required]}
                                        />                                       
                                        <Field
                                            id="meta_title"
                                            name="meta_title"
                                            label="Meta Title"
                                            type="textarea"                                                                                      
                                            component={renderField}                                           
                                            validate={[required]}
                                        />                                        
                                       
                                        
                                        <Field
                                            id="meta_description"
                                            name="meta_description"
                                            label="Meta Description"
                                            type="textarea"
                                            component={renderTextArea}                                           
                                            validate={[required]}
                                            
                                        />
                                        <Field
                                            id="keyword"
                                            name="keyword"
                                            label="Keyword"
                                            type="textarea"
                                            component={renderTextArea}
                                            validate={[required]}

                                        />
                                        {/*<Field
                                            id="meta_description"
                                            className="form-control"
                                            name="meta_description"
                                            rows="3"
                                            cols="125"
                                            component="textarea"
                                            validate={[required]}
                                        />
                                        
                                        <label className="control-label">Keyword</label>
                                        <Field 
                                            id="keyword" 
                                            className="form-control" 
                                            name="keyword" 
                                            rows="3" 
                                            cols="125" 
                                            component="textarea" 
                                            validate={[required]}                                          
                                        />*/}
 
                                    </div>
                                    <div className="col-md-2">
                                        <a href="/admin/#/seo" className="btn btn-primary pull-right">Back To Car Seo </a>
                                    </div>
                                </Row>
                                <button type="submit" className="btn-fill pull-right btn btn-info">Edit</button>
                                <div className="clearfix"></div>
                            </form>
                        }
                    />
                </Col>
            </Row>
        </Grid>
    )
}
EditSeo = reduxForm({
    form: 'editSeoForm',

    enableReinitialize: true
})(EditSeo)
export default EditSeo;
