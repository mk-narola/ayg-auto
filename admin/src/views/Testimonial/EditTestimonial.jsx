import React, { Component } from 'react';
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';
import { Field, reduxForm, initialize } from "redux-form";

import Loader from "../../components/Loader/Loader";
import { Card } from 'components/Card/Card.jsx';
import Button from 'elements/CustomButton/CustomButton.jsx';
import _ from 'lodash';
const imagePath = 'http://aygauto.com/backend/public/testimonial_images/';
//const imagePath = 'http://localhost/ali/ayg-auto/backend/public/testimonial_images/';
const required = value => value
    ? undefined
    : 'This Field Is Required.'
const renderField = ({ id, input, label, type, meta: { touched, error, warning }, disabled }) => {
    const errorClass = ((touched) && error) ? 'form-group has-error' : 'form-group';
    return (
        <div className={errorClass}>
            <label className="control-label">{label} <span className="required">*</span></label>
            {(type == "text") ? <input id={id} {...input} placeholder={label} type={type} className="form-control" disabled={disabled} /> : <textarea {...input} className="form-control" ></textarea>}
            {touched && ((error && <p className="help-block">{error}</p>) || (warning && <p className="help-block">{warning}</p>))}
        </div>
    )
}
const renderFile = ({ id, input, label, type }) => {
    let newInput = _.omit(input, ['value'])
    return (
        <div>
            <label className="control-label"> {label} </label>
            <input id={id} {...newInput} type={type} className="form-control" />
        </div>
    )
}

let EditTestimonial = (props) => {
    
    const { handleSubmit, pristine, reset, submitting } = props

    return (
        <Grid fluid>
            <Row>
                <Col md={8} mdOffset={2}>
                    <Card
                        title="Edit Testimonial"
                        content={                       
                            <form id="EditTestimonial" onSubmit={handleSubmit(props.onSubmit)} method="post" encType="multipart/form-data" >
                                <Row>
                                    <div className="col-md-8">
                                        <Field
                                            id="name"
                                            name="title"
                                            label="Testimonial Title"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                        />
                                        <Field
                                            id="description"
                                            name="description"
                                            label="Testimonial Description"
                                            type="textarea"
                                            component={renderField}
                                            validate={[required]}
                                        />
                                        <Field
                                            id="image"
                                            name="file"
                                            type="file"
                                            name="image"
                                            label="Testimonial Image"
                                            component={renderFile} />
                                        <img height="100px" width="100px" src={`${imagePath}${props.initialValues.image}`} />
                                    </div>
                                    <div className="col-md-4">
                                        <a href="/admin/#/testimonial" className="btn btn-primary pull-right">Back To Testimonial </a>
                                    </div>
                                </Row>
                                <button type="submit" className="btn-fill pull-right btn btn-info">Edit</button>
                                <div className="clearfix"></div>
                            </form>
                        }
                    />
                </Col>
            </Row>
        </Grid>
    )
}
EditTestimonial = reduxForm({
    form: 'EditTestimonial',
    enableReinitialize: true
})(EditTestimonial)

export default EditTestimonial;
