/**
*
* DropDownSelect
*
*/

import React from 'react';
const required = value => value
  ? undefined
  : 'This Field Is Required.'
class DropDownSelect extends React.Component { // eslint-disable-line react/prefer-stateless-function
  
  renderSelectOptions = (person) => (
    <option key={person} value={person} >{person}</option>
  )

  render() {
    const { input, label } = this.props;
    return (
      <div>
        {/* <label htmlFor={label}>{label}</label> */}
        <select {...input} className="form-control" 
          required>
          <option value="">Select</option>
          {this.props.categories.map(this.renderSelectOptions)}
        </select>
      </div>
    );
  }
}

DropDownSelect.propTypes = {
  people: React.PropTypes.array,
  input: React.PropTypes.object,
  label: React.PropTypes.string,
};

export default DropDownSelect;