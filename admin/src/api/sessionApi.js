import * as apiConstant from './apiConstant'
class sessionApi {
    static login(credentials) {        
        return fetch(`${apiConstant.API_URL}login`, {
            method: 'post',
            headers: {
              "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: `email=${credentials.username}&password=${credentials.password}`
          })
          .then(function (response) {
            return response.json();
          })
          .catch(function (error) {
            return error;
          });
    }
}

export default sessionApi;