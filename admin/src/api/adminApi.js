import * as apiConstant from './apiConstant'

class adminApi {
    static fetchUser() {
        return fetch(`${apiConstant.API_URL}me?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
        }).then(function (response) {
                return response.json();
        }).catch(function (error) {
                throw error;
        });
    }
    static updateUser(req){
        return fetch (`${apiConstant.API_URL}profile`,{
           method: 'post',
            headers: {
              "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: `email=${req.email}&password=${req.password}&token=${sessionStorage.getItem('jwt')}`
          })
          .then(function (response) {
            return response.json();
          })
          .catch(function (error) {
            return error;
          }); 
    }
}

export default adminApi