import * as apiConstant from './apiConstant'
import _ from 'lodash'

class carApi {

    static addCategory(req) {
        var input = document.querySelector('input[type="file"]')
        var data = new FormData()
        data.append('file', input.files[0])        
        data.append('category_name', req.category_name);
        //console.log("add data",data);
        return fetch(`${apiConstant.API_URL}add-categories`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',                                            
                "Authorization": "Bearer " + sessionStorage.getItem('jwt')
            },
                body: data
            })
            .then(function (response) {
                return response.json();
            })
            .catch(function (error) {
                return error;
            });
    }

    static editCategory(req) {
        var input = document.querySelector('input[type="file"]');
        
        var data = new FormData()
        if(typeof input.files[0] != 'undefined' && input.files[0] != null)
        {
            data.append('file', input.files[0]);            
        }
        else
        {
            data.append('image',req.image); 

        }    
        data.append('id',  req.id);       
        data.append('category_name', req.category);
       
        return fetch(`${apiConstant.API_URL}edit-categories`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',                                            
                "Authorization": "Bearer " + sessionStorage.getItem('jwt')
            },
                body: data
            })
            .then(function (response) {
                return response.json();
            })
            .catch(function (error) {
                return error;
            });
    }

    static getCategories(){
        return fetch(`${apiConstant.API_URL}get-categories?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }


    static deleteCategories(id){        
        return fetch(`${apiConstant.API_URL}delete-categories/${id}?token=${sessionStorage.getItem('jwt')}`, {
            method: 'delete',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }
    
    static getCategories_by_id(id){
        return fetch(`${apiConstant.API_URL}get-categories-by-id/${id}?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }

    static addCar(req){
        let form = document.getElementById('addCar');
        let formData = new FormData(form);
        return fetch(`${apiConstant.API_URL}add-car?token=${sessionStorage.getItem('jwt')}`, {
            method: 'post',            
            body: formData
            })
            .then(function (response) {
                return response.json();
            })
            .catch(function (error) {
                return error;
            });
    }

    static editCar(req){
        let form = document.getElementById('addCar');
        let formData = new FormData(form);
        var input = document.querySelector('input[type="file"]');      
        var data = formData;
        if(typeof input.files[0] != 'undefined')
        {
            data.append('thumbnail', input.files[0]['name']);           
            //console.log("slider_imagesiiiii",req.slider_images);           
        }
        else
        {
            data.append('thumbnail',req.thumbnail);
            data.append('slider_images',req.slider_images); 
            //console.log(req.slider_images,req.thumbnail,req.id);
        }
        data.append('id',  req.id);       
        data.append('car_id',  req.car_id);       
         
        return fetch(`${apiConstant.API_URL}edit-car?token=${sessionStorage.getItem('jwt')}`, {
            method: 'post',
                body: data
            })
            .then(function (response) {
                return response.json();
            })
            .catch(function (error) {
                return error;
            });
    }

    static deleteCars(car_id){       
        return fetch(`${apiConstant.API_URL}delete-car/${car_id}?token=${sessionStorage.getItem('jwt')}`, {
            method: 'delete',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }

    static getCars(){
        return fetch(`${apiConstant.API_URL}get-cars?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
           
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }

    static getCar_by_id(id){
        return fetch(`${apiConstant.API_URL}getCar-by-id/${id}?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
            
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }

    static deleteImage(id,imgid){
        return fetch(`${apiConstant.API_URL}delimg-by-id/${id}/${imgid}?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
            
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }

   

    static getStocks() {
        return fetch(`${apiConstant.API_URL}get-stocks?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',

        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }

    static getCarsStocks() {
        return fetch(`${apiConstant.API_URL}get-cars-stocks?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',

        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }
    

    static updateStocks(req) {
        var data = new FormData()
        data.append('car_id', req.car_id);
        data.append('available', req.available);
        //console.log("StockForm",data);
        return fetch(`${apiConstant.API_URL}update-stock?token=${sessionStorage.getItem('jwt')}`, {
            method: 'post',
            body: data
        })
            .then(function (response) {
                return response.json();
            })
            .catch(function (error) {
                return error;
            });
    }
    static getTestimonial() {
        return fetch(`${apiConstant.API_URL}get-all-testimonial?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }
    static deleteTestimonial(id) {
        return fetch(`${apiConstant.API_URL}deletemonial-by-id/${id}?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }
    static testimonialById(id) {
        return fetch(`${apiConstant.API_URL}testimonial-by-id/${id}?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }

    static addTestimonial(req) {
        var input = document.querySelector('input[type="file"]')
        var data = new FormData() 
        if (typeof input.files[0] != 'undefined' && input.files[0] != null) {
           
        }      
        data.append('title', req.title);
        data.append('description', req.description);
        data.append('file', input.files[0])
       
        return fetch(`${apiConstant.API_URL}add-testimonial`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('jwt')
            },
            body: data
        })
            .then(function (response) {
                return response.json();
            })
            .catch(function (error) {
                return error;
            });
    }
    static editTestimonial(req) {
        var input = document.querySelector('input[type="file"]');

        var data = new FormData()
        if (typeof input.files[0] != 'undefined' && input.files[0] != null) {
            data.append('file', input.files[0]);
        }
        else {
            data.append('image', req.image);

        } 
           
        data.append('id', req.id);
        data.append('title', req.title);
        data.append('description', req.description);
        //console.log(req.id, req.title, req.description, req.image);
        return fetch(`${apiConstant.API_URL}edit-testimonial`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('jwt')
            },
            body: data
        })
            .then(function (response) {
                return response.json();
            })
            .catch(function (error) {
                return error;
            });
    }
}

export default carApi