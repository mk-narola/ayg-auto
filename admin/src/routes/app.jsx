import Dashboard from 'views/Dashboard/Dashboard';
import UserProfile from '../containers/Account/User';
import CarsMaster from 'views/CarsMaster/CarsMaster';
import SeoMaster from 'views/SeoMaster/SeoMaster';
import AddCar from '../containers/Car/AddCarPage';
import EditCar from '../containers/Car/EditCarPage';
import CarCategories  from "../containers/Car/Categories";
import AddCarCategory  from "../containers/Car/AddCategory";
import EditCarCategory  from "../containers/Car/EditCategory";
import EditSeo from "../containers/Seo/EditSeo";
import Typography from 'views/Typography/Typography';
import Icons from 'views/Icons/Icons';
import inventory from 'views/Inventory/index';
import testimonial from 'views/Testimonial/Testimonial';
import AddTestimonial from '../containers/Testimonial/AddTestimonial';
import EditTestimonial from '../containers/Testimonial/EditTestimonial';
import Maps from 'views/Maps/Maps';
import Notifications from 'views/Notifications/Notifications';
import Upgrade from 'views/Upgrade/Upgrade';


const appRoutes = [
    { path: "/dashboard", name: "Dashboard", icon: "pe-7s-graph", component: Dashboard },
    { path: "/profile", name: "Admin Profile", icon: "pe-7s-user", component: UserProfile },
    { path: "/cars", name: "Car Master", icon: "pe-7s-car", component: CarsMaster },
    { path: "/seo", name: "Seo Master", icon: "pe-7s-ticket", component: SeoMaster },
    { path: "/seo-edit/:id", name: "Seo Edit", icon: "pe-7s-ticket", component: EditSeo, isHide: true },
    { path: "/car/new", name: "Car Master", icon: "pe-7s-car", component: AddCar,isHide: true },
    { path: "/car/:id", name: "Car Master", icon: "pe-7s-car",  component: EditCar,isHide: true },
    { path: "/car-categories",name:'Car Categories',icon: "pe-7s-news-paper",component:CarCategories },
    { path: "/car-category/new",name:'Add Car Category',icon: "pe-7s-news-paper",component:AddCarCategory,isHide: true },
    { path: "/car-category/:id",name:'Edit Car Category',icon: "pe-7s-news-paper",component:EditCarCategory,isHide: true },
    { path: "/inventory", name: "Inventory", icon: "pe-7s-notebook", component: inventory},
    { path: "/testimonial", name: "Testimonial", icon: "pe-7s-user", component: testimonial },
    { path: "/testi-monial/new", name: 'Add Testimonial Category', icon: "pe-7s-user", component: AddTestimonial, isHide: true },
    { path: "/testi-monial/:id", name: 'Edit Testimonial Category', icon: "pe-7s-user", component: EditTestimonial, isHide: true },
    // { path: "/typography", name: "Typography", icon: "pe-7s-news-paper", component: Typography },
    //{ path: "/icons", name: "Icons", icon: "pe-7s-science", component: Icons },
    // { path: "/maps", name: "Maps", icon: "pe-7s-map-marker", component: Maps },
    // { path: "/notifications", name: "Notifications", icon: "pe-7s-bell", component: Notifications },
    { redirect: true, path:"/", to:"/dashboard", name: "Dashboard" }
];

export default appRoutes;
