<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('profile', 'App\\Api\\V1\\Controllers\\UserController@update');
        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');
        $api->post('profile', 'App\\Api\\V1\\Controllers\\UserController@update');
		
        $api->get('get-categories', 'App\\Api\\V1\\Controllers\\CarCategoriesController@getCategories');
		$api->get('get-categories-by-id/{id}', 'App\\Api\\V1\\Controllers\\CarCategoriesController@getCategories_by_id');		
        $api->post('add-categories', 'App\\Api\\V1\\Controllers\\CarCategoriesController@addCategories');
		$api->post('edit-categories', 'App\\Api\\V1\\Controllers\\CarCategoriesController@editCategories');
		$api->delete('delete-categories/{id}', 'App\\Api\\V1\\Controllers\\CarCategoriesController@deleteCategories');
		
        $api->post('add-car', 'App\\Api\\V1\\Controllers\\CarsController@addCar');
		$api->post('edit-car', 'App\\Api\\V1\\Controllers\\CarsController@editCar');
        $api->get('get-cars','App\\Api\\V1\\Controllers\\CarsController@getAllCars');
		$api->get('getCar-by-id/{id}','App\\Api\\V1\\Controllers\\CarsController@getCar_by_id');
        $api->get('get-car-details','App\\Api\\V1\\Controllers\\CarsController@getCarDetails');
        $api->delete('delete-car/{id}','App\\Api\\V1\\Controllers\\CarsController@deleteCar');
        $api->get('delimg-by-id/{id}/{imgid}','App\\Api\\V1\\Controllers\\CarsController@deleteImg');
        $api->get('get-stocks','App\\Api\\V1\\Controllers\\CarsController@getStockDetails');
        $api->post('update-stock', 'App\\Api\\V1\\Controllers\\CarsController@updateStock');
        $api->get('get-cars-stocks','App\\Api\\V1\\Controllers\\CarsController@getAllCarsStock');
        $api->get('testimonial-by-id/{id}','App\\Api\\V1\\Controllers\\CarsController@testimonial_by_id');
        $api->get('deletemonial-by-id/{id}','App\\Api\\V1\\Controllers\\CarsController@deleteTestimonial');
        $api->get('get-all-testimonial','App\\Api\\V1\\Controllers\\CarsController@getAlltestimonial');
        $api->post('add-testimonial', 'App\\Api\\V1\\Controllers\\CarsController@addTestimonial');
        $api->post('edit-testimonial', 'App\\Api\\V1\\Controllers\\CarsController@editTestimonial');


    });
    
    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->get('get-categoires-na','App\\Api\\V1\\Controllers\\CarCategoriesController@getAllCategories');
    $api->get('get-brands-na','App\\Api\\V1\\Controllers\\CarsController@getAllBrands');
    $api->get('get-brand-models-na','App\\Api\\V1\\Controllers\\CarsController@getBrandModels');
    $api->get('get-cars-listing-na','App\\Api\\V1\\Controllers\\CarsController@getListing');
    $api->get('get-car-na','App\\Api\\V1\\Controllers\\CarsController@getCarData');
    $api->post('get-cars-home-fliter','App\\Api\\V1\\Controllers\\CarsController@getCarFliterHome');
    $api->post('sent-mail','App\\Api\\V1\\Controllers\\CarsController@sentMail');
    $api->get('get-all-testimonials','App\\Api\\V1\\Controllers\\CarsController@getAlltestimonials');
    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});
