<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarCategories extends Model
{
    // fillable
    protected $fillable = ['category','image'];
}
