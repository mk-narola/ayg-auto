<?php

namespace App\Api\V1\Controllers;

use App\CarCategories;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use JWTAuth;

class CarSeoController extends Controller
{

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function getCategories_by_id($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $categoties = CarCategories::findOrFail($id);
            return response()->json($categoties);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }
    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $categoties = CarCategories::all();
            return response()->json($categoties);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }
    /**
     * FRONT END || MAIN SITE METHOD || NOT AUTH 
     * ALL CATEGORIES
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCategories()
    {
        $categoties = CarCategories::all();
        if($categoties){
            return response()->json($categoties);
        }else{
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Something Went Wrong! CAR CATEGOIRES',
            ]);
        }
        
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCategories(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $image = $request->file('file');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('ayg_images');
            $image->move($destinationPath, $imageName);
            $catData = new CarCategories([
                'category' => $request->get('category_name'),
                'image' => $imageName,
            ]);
            $catData->save();
            return response()->json([
                'status' => 'success',
                'message' => 'CarCategory Uploaded Successfuly',
            ]);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editCategories(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $id = $request->get('id');
            if ($request->hasFile('file')) {
                $image = $request->file('file');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('ayg_images');
                $image->move($destinationPath, $imageName);
            } else {
                $imageName = $request->get('image');
            }
            $catData = CarCategories::findOrFail($id);
            $catData->category = $request->get('category_name');
            $catData->image = $imageName;

            $catData->save();
            return response()->json([
                'status' => 'success',
                'message' => 'CarCategories Successfuly Updated',
            ]);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }
    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCategories($id)
    {
        $data = $id;
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            DB::table('car_categories')->where('id', $id)->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Deleted  data',
            ]);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }
    }

}
