<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
//use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\User;
use Auth;
use JWTAuth;

class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        return response()->json(Auth::guard()->user());
    }

    public function update(Request $request)
	{
        $currentUser = JWTAuth::parseToken()->authenticate();

	    $user = User::where('email', '=', $request->get('email'))->first();	    
        if(!$user)
	        throw new NotFoundHttpException;

	    $user->fill($request->all());

	    if($user->save())
	        return response()->json([	        	
	            'status' => 'ok',
	            'message' => 'Profile updated successfully.'	            
	        ]);
	    else
	        return response()->json([	        	
	            'status' => 'error'
	        ]);
	}	
}
