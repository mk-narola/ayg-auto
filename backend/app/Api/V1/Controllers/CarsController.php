<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use DB;
use App\Mail\WelcomEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use JWTAuth;
use Image;
class CarsController extends Controller
{

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCars()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $cars = DB::table('cars')->get();
            return response()->json($cars);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCar_by_id($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $cars = DB::table('cars')->join('car_details', 'cars.id', '=', 'car_details.car_id')
                ->where('cars.id', $id)->first();
            if ($cars) {
                return response()->json($cars);
            } else {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => 'SOMETHING WENT WRONG',
                ]);
            }

        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

        /*$currentUser = JWTAuth::parseToken()->authenticate();
    if ($currentUser) {
    $carBasic = DB::select('select * from cars where id = ?',$id);
    $carDetails = DB::select('select * from car_details where car_id = ?',$id);

    $carAll = ['car_bacic' => $carBasic[0], 'carDetails' => $carDetails[0]];
    return response()->json([
    'status' => 'success',
    'car_data' => $carAll,
    ]);
    } else {
    return response()->json([
    'status' => 'ERROR',
    'message' => 'AUTH ISSUES || H$R#EN',
    ]);
    }*/
    }

    public function deleteImg($id, $imgid)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $slider = [];
            $cars = DB::table('car_details')->select('slider_images')->where('car_details.car_id', $id)->first();

            $slider = json_decode($cars->slider_images);
            unset($slider[$imgid]);
            $slider = array_values($slider);

            $cardetails = DB::table('car_details')->where('car_details.car_id', $id)->update([
                'slider_images' => json_encode($slider),
            ]);

            return response()->json([
                'status' => 'success',
                'message' => "Your Image is succesfully deleted",
            ]);
            //return response()->json($cardetails);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }

    public function addCar(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $status = '';
            $destinationPath = public_path('cars_images');
            $image = $request->file('thumbnail');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $imageName);
            $image1 = $request->file('images');
            $sliderName = [];
            if ($request->hasFile('images')) {
                $files = $request->file('images');
                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $picture = date('His') . $filename;
                    if ($file->move($destinationPath, $picture)) {
                        array_push($sliderName, $picture);
                    }
                }
            }
            $carId = DB::table('cars')->insertGetId(
                ['car_categories' => $request->get('car_categories'),
                    'brand' => $request->get('brand'),
                    'model_name' => $request->get('model_name'),
                    'price' => $request->get('price'),
                    'year' => $request->get('year'),
                    'miles' => $request->get('miles'),
                    'car_color' => $request->get('car_color'),
                    'car_transmission' => $request->get('car_transmission'),
                    'thumbnail' => $imageName,
                    'car_type'	=> $request->get('car_type'),
                    //'' => date('Y-m-d H:i:s');
                ]
            );
            $otherInfo = ['accessory_packages' => $request->get('accessory_packages'),
                'accessory_packages' => $request->get('accessory_packages'),
                'braking_traction' => $request->get('braking_traction'),
                'comfort_convenience' => $request->get('comfort_convenience'),
                'entertainment_instrumentation' => $request->get('entertainment_instrumentation'),
                'exterior' => $request->get('exterior'),
                'lighting' => $request->get('lighting'),
                'safety_security' => $request->get('safety_security'),
                'seats' => $request->get('seats'),
                'steering' => $request->get('steering'),
                'video_urls' => $request->get('video_urls'),
                'wheels_tires' => $request->get('wheels_tires')];

            //$catData = CarCategories::findOrFail($id);
            $carDetailsID = DB::table('car_details')->insertGetId([
                'car_id' => $carId,
                'passenger_capacity' => $request->get('passenger_capacity'),
                'wheel' => $request->get('wheel'),
                'tire' => $request->get('tire'),
                'powertrain_warranty' => $request->get('powertrain_warranty'),
                'engine' => $request->get('engine'),
                'horsepower' => $request->get('horsepower'),
                'transmission' => $request->get('transmission'),
                'stock_number' => $request->get('stock_number'),
                'vin' => $request->get('vin'),
                'mpg_city' => $request->get('mpg_city'),
                'mpg_hwy' => $request->get('mpg_hwy'),
                'drivetrain' => $request->get('drivetrain'),
                'other_information' => json_encode($otherInfo),
                'slider_images' => json_encode($sliderName),
                'video_urls' => ($request->get('video_urls') ? implode('||', $request->get('video_urls')) : ''),
            ]);
            $carStockID = DB::table('cars_stock')->insertGetId([
                'car_id' => $carId,
                'available' => 'Y',
            ]);
            if ($imageName && $carId && $carStockID) {
                $status = 'success';
                return response()->json([
                    'status' => $status,
                    'message' => 'Car Uploaded Successfuly With Basic Data Only & Add As Available In Stock',
                ]);
            }
            if ($imageName && $carId && $carDetailsID && $sliderName && $carStockID) {
                $status = 'success';
                return response()->json([
                    'status' => $status,
                    'message' => 'Car Uploaded Successfuly & Add As Available In Stock',
                ]);
            } else {
                return response()->json([
                    'status' => $status,
                    'message' => 'Car Upload ISSUES || SOMETHING WENT WRONG',
                ]);
            }
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }

    public function editCar(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        if ($currentUser) {

            //Edit the Car Information
            if ($request->hasFile('thumbnail')) {
                $destinationPath = public_path('cars_images');
                $image = $request->file('thumbnail');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $imageName);
            } else {
                $imageName = $request->get('thumbnail');
            }

            $id = $request->get('car_id');
            $car = DB::table('cars')->where('id', $id)->update([
                'car_categories' => $request->get('car_categories'),
                'brand' => $request->get('brand'),
                'model_name' => $request->get('model_name'),
                'price' => $request->get('price'),
                'year' => $request->get('year'),
                'miles' => $request->get('miles'),
                'car_color' => $request->get('car_color'),
                'car_transmission' => $request->get('car_transmission'),
                'thumbnail' => $imageName,
                'car_type'	=> $request->get('car_type'),

            ]);
            //Get OTher Information Values
            function check($var)
            {

                return (is_array($var) ? array_values(array_filter($var)) : $var);

            }
            $otherInfo = [
                'accessory_packages' => check($request->get('accessory_packages')),
                'braking_traction' => check($request->get('braking_traction')),
                'comfort_convenience' => check($request->get('comfort_convenience')),
                'entertainment_instrumentation' => check($request->get('entertainment_instrumentation')),
                'exterior' => check($request->get('exterior')),
                'lighting' => check($request->get('lighting')),
                'safety_security' => check($request->get('safety_security')),
                'seats' => check($request->get('seats')),
                'steering' => check($request->get('steering')),
                'video_urls' => check($request->get('video_urls')),
                'wheels_tires' => check($request->get('wheels_tires')),
            ];

            $sliderName = [];
            $temp = [];
            if ($request->hasFile('images')) {
                $files = $request->file('images');
                $destinationPath = public_path('cars_images');
                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $picture = date('His') . $filename;
                    if ($file->move($destinationPath, $picture)) {
                        array_push($sliderName, $picture);
                    }
                }
                $temp = explode(",", $request->get('slider_images'));
                $sliderName = array_merge($sliderName, $temp);
            } else {
                $sliderName = explode(",", $request->get('slider_images'));
            }

            //Update Car Information
            $car_id = $request->get('id');
            $cardetails = DB::table('car_details')->where('id', $car_id)->update([
                'passenger_capacity' => $request->get('passenger_capacity'),
                'wheel' => $request->get('wheel'),
                'tire' => $request->get('tire'),
                'powertrain_warranty' => $request->get('powertrain_warranty'),
                'engine' => $request->get('engine'),
                'horsepower' => $request->get('horsepower'),
                'transmission' => $request->get('transmission'),
                'stock_number' => $request->get('stock_number'),
                'vin' => $request->get('vin'),
                'mpg_city' => $request->get('mpg_city'),
                'mpg_hwy' => $request->get('mpg_hwy'),
                'drivetrain' => $request->get('drivetrain'),
                'other_information' => json_encode($otherInfo),
                'slider_images' => json_encode($sliderName),
                'video_urls' => ($request->get('video_urls') ? implode('||', $request->get('video_urls')) : ''),
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Car Updated Successfuly',
            ]);

        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }

    public function getCarDetails(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            //DB::table('cars')->join('car_details')
            $carBasic = DB::select('select * from cars where id = ?', [$request->get('car_id')]);
            $carDetails = DB::select('select * from car_details where car_id = ?', [$request->get('car_id')]);

            $carAll = ['car_bacic' => $carBasic[0], 'carDetails' => $carDetails[0]];
            return response()->json([
                'status' => 'success',
                'car_data' => $carAll,
            ]);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }
    }

    public function deleteCar($id)
    {
        $data = $id;
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            DB::table('cars')->where('id', $id)->delete();
            DB::table('car_details')->where('car_id', $id)->delete();
            DB::table('cars_stock')->where('car_id', $id)->delete();
            return response()->json([
                'status' => 'success',
                'message' => "Your Data is succesfully deleted",
            ]);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }
    }
    /*
    //  * ADMIN METHOD
    //  * GET ALL STOCK
    //  * @return \Illuminate\Http\JsonResponse
    //  */

    public function getStockDetails(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $carInStock = DB::select('select count(*) as count from cars_stock where available = "Y"');
            $carOutOfStock = DB::select('select count(*) as count from cars_stock where available = "N"');
            $stock = array(
                'instock' => $carInStock[0]->count,
                'outstock' => $carOutOfStock[0]->count,
            );
            if ($carInStock & $carOutOfStock) {
                return response()->json($stock);
            } else {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => 'NO DATA FOUND FOR STOCK',
                ]);
            }
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }
    }

    /*
     * ADMIN METHOD
     * UPDATE STOCK
     * @return \Illuminate\Http\JsonResponse
     */

    public function updateStock(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $stock = DB::table('cars_stock')->where('car_id', $request->get('car_id'))->update(
                ['available' => $request->get('available')]);
            if ($stock) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Stock Updated Successfully',
                ]);
            }
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCarsStock()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $cars = DB::table('cars')->join('cars_stock', 'cars.id', '=', 'cars_stock.car_id')->get();
            return response()->json($cars);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }

/**
 * FRONT END || MAIN SITE METHOD || NOT AUTH
 * GET ALL BRANDS
 * @return \Illuminate\Http\JsonResponse
 */
    public function getAllBrands()
    {
        $carBrands = DB::select('select DISTINCT brand from cars');
        if ($carBrands) {
            $brandSendAble = [];
            foreach ($carBrands as $key => $brand) {
                # code...
                $brandSendAble[] = $brand->brand;
            }
            return response()->json($brandSendAble);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Something Went Wrong! CAR BRANDS',
            ]);
        }
    }
/**
 * FRONT END || MAIN SITE METHOD || NOT AUTH
 * GET ALL MODELS OF ONE BRAND
 * @return \Illuminate\Http\JsonResponse
 */
    public function getBrandModels(Request $request)
    {
        $carModels = DB::select('select DISTINCT model_name from cars where brand = ?', [$request->get('brand')]);
        if ($carModels) {
            $modelSendAble = [];
            foreach ($carModels as $key => $model) {
                # code...
                $modelSendAble[] = $model->model_name;
            }
            return response()->json($modelSendAble);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Something Went Wrong! CAR BRANDS',
            ]);
        }
    }
    /**
     * FRONT END || MAIN SITE METHOD || NOT AUTH
     * LISTING PAGE FLITER QUERY
     * @return \Illuminate\Http\JsonResponse
     */

    public function getListing(Request $request)
    {
        $where = [];
        $whereBetween = [];
        if ($request->get('car_categories')) {
            $where[] = ['car_categories', '=', $request->get('car_categories')];
        }
        if ($request->get('brand')) {
            $where[] = ['brand', '=', $request->get('brand')];
        }
        if ($request->get('model_name')) {
            $where[] = ['model_name', '=', $request->get('model_name')];
        }
        if ($request->get('car_color')) {
            $where[] = ['car_color', '=', $request->get('car_color')];
        }
        if ($request->get('car_transmission')) {
            $where[] = ['car_transmission', '=', $request->get('car_transmission')];
        }
        if ($request->get('price_max') && $request->get('price_min')) {
            $whereBetween[] = ['price', [$request->get('price_min'), $request->get('price_max')]];
        }
        if ($request->get('miles_max') && $request->get('miles_min')) {
            $whereBetween[] = ['miles', [$request->get('miles_min'), $request->get('miles_max')]];
        }
        if ($request->get('year_max') && $request->get('year_min')) {
            $whereBetween[] = ['year', [$request->get('year_min'), $request->get('year_max')]];
        }
        $where[] = ['available','=','Y'];
        $carModels = DB::table('cars')->select('cars.*')->join('cars_stock', 'cars.id', '=', 'cars_stock.car_id')->where($where);
        if (count($where) > 0) {
            $carModels->where($where);
        }
        if (count($whereBetween) > 0) {
            $carModels->whereBetween($whereBetween);
        }
        $carModels = $carModels->get();

        if ($carModels) {
            return response()->json($carModels);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Something Went Wrong! CAR FLITER',
            ]);
        }
    }

    /**
     * FRONT END || MAIN SITE METHOD || NOT AUTH
     * Single Car
     * @return \Illuminate\Http\JsonResponse
     */

    public function getCarData(Request $request)
    {
        //\DB::enableQueryLog();
        $cars = DB::table('cars')->select("cars.*","car_details.passenger_capacity","car_details.wheel","car_details.tire","car_details.powertrain_warranty","car_details.engine","car_details.horsepower","car_details.transmission","car_details.stock_number","car_details.vin","car_details.mpg_city","car_details.mpg_hwy","car_details.drivetrain","car_details.other_information","car_details.slider_images","car_details.video_urls")->join('car_details', 'cars.id', '=', 'car_details.car_id')
            ->where('cars.id', $request->get('id'))->first();
           // $query = \DB::getQueryLog();
        //print_r(end($query));
        //die; 
        //var_dump($request->get('id'));
        $cars->slider_images = json_decode($cars->slider_images);
        $cars->other_information = json_decode($cars->other_information);
       
       // die();    
       // $cars->slider_images = json_decode(isset($cars->slider_images)?$cars->slider_images:[]);
       // $cars->other_information = json_decode(isset($cars->other_information)?$cars->other_information:[]);
        //print_r($cars->toArray());
        //die();
        return response()->json($cars);
    }

    public function getCarFliterHome(Request $request)
    {
        $where = [];
        $whereBetween = [];

        if ($request->get('car_categories')) {
            $where[] = ['car_categories', '=', $request->get('car_categories')];
        }

        if ($request->get('car_brand')) {
            $where[] = ['brand', '=', $request->get('car_brand')];
        }

        if ($request->get('car_model')) {
            $where[] = ['model_name', '=', $request->get('car_model')];
        }

        if ($request->get('select_year')) {
            $where[] = ['year', '=', $request->get('select_year')];
        }
        if($request->get('select_car_type')){
             $where[] = ['car_type', '=', $request->get('select_car_type')];
        }
        $where[] = ['available','=','Y'];
        $carModels = DB::table('cars')->select("cars.*","cars_stock.available")->join('cars_stock', 'cars.id', '=', 'cars_stock.car_id')->where($where);

        if (count($where) > 0) {
            $carModels->where($where);
        }
        if (count($whereBetween) > 0) {
            $carModels->whereBetween($whereBetween);
        }

        $carModels = $carModels->get();

        if ($carModels) {
            return response()->json($carModels);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Something Went Wrong! CAR FLITER',
            ]);
        }
    }
    public function sentMail(Request $request)
    {
       print_r($request->all());
       
       $data = $request->all();
        //$email_id="aygautoinc@gmail.com";
      	$email_id="ritikapatel3500@gmail.com";
       Mail::to($email_id)->send(new WelcomeMail($data));
       if($data)
       {
        return response()->json([
                'status' => 'success',
                'message' => "Your Mail Is Send",
            ]);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Some Issue!!!!',
            ]);
        }
       
    }
    public function getAlltestimonials()
    {
       
        $testimonial = DB::table('testimonial')->get();
        if($testimonial)
        {
        return response()->json($testimonial);
    	}
         else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }
 /**
  * [testimonial_by_id description]
  * @param  [type] $id [description]
  * @return [type]     [description]
  */
    public function testimonial_by_id($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $testimonial = DB::table('testimonial')->where('testimonial.id', $id)->first();
            if ($testimonial) {
                return response()->json($testimonial);
            } else {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => 'SOMETHING WENT WRONG',
                ]);
            }

        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    
    }
    public function getAlltestimonial()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $testimonial = DB::table('testimonial')->get();
            return response()->json($testimonial);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }
    public function deleteTestimonial($id)
    {
        $data = $id;
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            DB::table('testimonial')->where('id', $id)->delete();            
            return response()->json([
                'status' => 'success',
                'message' => "Your Data is succesfully deleted",
            ]);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }
    }
     public function addTestimonial(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            if ($request->hasFile('file')) {
            $image = $request->file('file');
            $imageName = time() . '.' . $image->getClientOriginalExtension();            
            $destinationPath = public_path('testimonial_images');
            $thumb_img = Image::make($image->getRealPath())->resize(670,650);

            /* $image = $request->file('image');
            $imageName = time().'.'.$request->image->getClientOriginalExtension();   
            $destinationPath = public_path('gallery_images');*/
            //$thumb_img = Image::make($image->getRealPath())->resize(100, 100);
            $thumb_img->save($destinationPath.'/'.$imageName,100);
            }

            //$image->move($destinationPath, $imageName);
            $testimonial=DB::table('testimonial')->insert(
			    ['title' => $request->get('title'),
                'description' => $request->get('description'),
                'image' => $imageName]
			);            
            if($testimonial)
            {
            return response()->json([
                'status' => 'success',
                'message' => 'Testimonial Uploaded Successfuly',
            ]);
        	}
        } else {
            return response()->json([
                'status' => 'Something Went Error',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }

    public function editTestimonial(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $id = $request->get('id');
            if ($request->hasFile('file')) {
                $image = $request->file('file');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('testimonial_images');
                $thumb_img = Image::make($image->getRealPath())->resize(370,329);

                $thumb_img->save($destinationPath.'/'.$imageName,80);
                //$image->move($destinationPath, $imageName);
            } else {
                $imageName = $request->get('image');
            }
           
             $testimonial = DB::table('testimonial')->where('id', $id)->update(
             	['title' => $request->get('title'),
                'description' => $request->get('description'),
                'image' => $imageName]);
            
             if($testimonial){           

	            return response()->json([
	                'status' => 'success',
	                'message' => 'Testimonial Successfuly Updated',
	            ]);
        	}
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }

}
