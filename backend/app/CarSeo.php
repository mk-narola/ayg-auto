<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarSeo extends Model
{
	protected $table = 'car_seo';
    // fillable
    protected $fillable = ['route','meta_title','meta_description','keyword'];
}
