<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('car_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('passenger_capacity');
            $table->string('wheel');
            $table->string('tire');
            $table->string('powertrain_warranty');
            $table->string('engine');
            $table->string('horsepower');
            $table->string('transmission');
            $table->string('stock_number');
            $table->string('vin');
            $table->string('mpg_city');
            $table->string('mpg_hwy');
            $table->string('drivetrain');
            $table->mediumText('other_information');
            $table->string('slider_images');
            $table->string('video_urls');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
