import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import store from './store/configureStore'
import { Helmet } from "react-helmet";

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));


registerServiceWorker();


