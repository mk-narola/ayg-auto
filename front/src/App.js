import React, { Component } from 'react';
import { Redirect, Link, Route, Switch } from "react-router-dom";
import { withRouter } from "react-router";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Homepage from "./containers/Homepage";
import _ from "lodash";
import Products from "./containers/ProductListing";
import Product_Details from "./containers/ProductDetails";
import appRoutes from "./components/rout/app";
import { Helmet } from "react-helmet";
import './assets/css/bootstrap.css'
import './assets/css/style.css'
import './assets/css/responsive.css'
import './assets/css/bootstrap-slider.css'
import seoApi from "./api/seoApi"
const SITE_URL =
  process.env.NODE_ENV === 'development'
    ? 'http://localhost:3000'
    : 'http://www.aygauto.com';

class App extends Component {
  state = { seo_car: [], seo: [] };

  componentDidMount = () => {

    this.fetch();    
    seoApi.getCarSeo().then(response => {
    this.setState({ seo_car: response });
    });
  }
  fetch = () => {

    seoApi.getSeo().then(response => {
      this.setState({ seo: response });
    });

  }
  render() {
    const data = this.state.seo;
    var seo_car = this.props.location.pathname.split("/")
   
    let title = appRoutes[0].title
    let description = appRoutes[0].description
    let keyword = appRoutes[0].keyword
    if (typeof seo_car[2] === 'undefined')
    {
    let seo_index = _.findIndex(appRoutes, { 'path': this.props.location.pathname })
    this.state.seo.map(function (item, index) {
      if (seo_index >= 0 && seo_index === index) {
        title = item.meta_title
        description = item.meta_description
        keyword = item.keyword
      }
    })
    }
    else{
     
    this.state.seo_car.map(function (item, index) {
        if (item.car_id===seo_car[2]) {
          title = item.meta_title
          description = item.meta_description
          keyword = item.keyword          
        }     
    })   

  }


    return (

      <div className="main">       

        <Helmet
           htmlAttributes={{
            lang: 'en',
            itemscope: undefined,           
          }}
          title={title }
          
          /*link={[
            {
              rel: 'canonical',
              href: SITE_URL + this.props.location.pathname
            }
          ]}*/
          meta={[           
            {             
              name:"keyword",
              content: keyword
            },
            {
              name: "description",
              content: description
            }
            
          ]}
          
        />

        <Header />

        <Switch>
          {appRoutes.map((prop, key) => {
            return (
              <Route exact={prop.exact} path={prop.path} component={prop.component} key={key} />
            );
          })}

          {/* <Route exact path="/" component={Homepage}  />
              <Route exact path="/cars" component={Products}  /> */}
          {/* <Route exact path="/car-details/:id" component={Product_Details} /> */}
        </Switch>
        <Footer />
      </div>
    );
  }
}



export default withRouter(App);


