import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';

class Year extends Component {
    state = {
        value: { min: 2008, max: 2018 },
    };

    render() {
        return (
            <div className="year-range">
                <InputRange
                    maxValue={2018}
                    minValue={2008}
                    value={this.state.value}
                    onChange={value => this.setState({ value })} />
            </div>
        );
    }
}
export default Year;