import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';

class Price extends Component {
    state = {
        value: { min: 55000, max: 70000 },
    };

    render() {
        return (
            <div className="price-range">
                <InputRange
                    maxValue={80000}
                    minValue={35000}
                    value={this.state.value}
                    onChange={value => this.setState({ value })} />
            </div>
        );
    }
}
export default Price;