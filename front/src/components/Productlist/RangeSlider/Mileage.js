import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';

class Mileage extends Component {
    state = {
        value: { min: 5000, max: 100000 },
    };

    render() {
        return (
            <div className="mileage-range">
                <InputRange
                    maxValue={100000}
                    minValue={5000}
                    value={this.state.value}
                    onChange={value => this.setState({ value })} />
            </div>
        );
    }
}
export default Mileage;