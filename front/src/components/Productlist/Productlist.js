import React, { Component } from 'react';

import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import _ from 'lodash';
const imagePath = 'http://aygauto.com/backend/public/cars_images/';

class Productlist extends Component {
	
	render() {

		let countOfCars;
		const min = this.props.data.min;
		const max = this.props.data.max;
		const ymin = this.props.year.min;
		const ymax = this.props.year.max;
		const mmin = this.props.mileage.min;
		const mmax = this.props.mileage.max;
		
		(_.isArray(this.props.carsData)?countOfCars=this.props.carsData.length:"");
		
		return (			
				<div className="col-xl-9 col-12">
					<div className="row list-main">
						<div className="col-12">
							<span className="matche">{countOfCars} Matches</span>
							<div className="form-group select2 float-right">
								<label>Sort By :&nbsp;</label>
								<select id="select" className="form-control">
									<option value="Recommended">Recommended</option>
								</select>
							</div>
						</div>
					{ _.isArray(this.props.carsData) ? this.props.carsData.map((item, index) => (
						((item.price >= min && item.price <= max) && (item.year >= ymin && item.year <= ymax) && (item.miles >= mmin && item.miles <= mmax) ?
								<div className="col-lg-4 col-md-6 col-12" key={index}>
									<div className="list-box">
									<div className="img">
										<Link to={`/car-details/${item.id}`}>
										<img src={`${imagePath}${item.thumbnail}`} alt="" />
										<i className="fa fa-heart-o wishlist-icon" />
										</Link>
									</div>
									<h5>
										{item.year} {item.brand}
									</h5>
									<h4>
										<span className="price float-right">${item.price}</span>
										<span className="title">{item.model_name}</span>
										<span>{item.car_transmission}</span>
									</h4>
									</div>
								</div>:'')
						)):""}
						<div className="col-12 text-center">
							<span className="results"> End of Results </span>
						</div>
					</div>
				</div>
		
		);
	}
}

export default Productlist;