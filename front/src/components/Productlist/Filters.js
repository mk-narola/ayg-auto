import React, { Component } from "react";
import Price from './RangeSlider/Price';
import Year from './RangeSlider/Year';
import Mileage from './RangeSlider/Mileage';
import BodyStyle from './BodyStyle';

import 'react-input-range/lib/css/index.css';

class Filters extends Component {
    render() {
        
    return (
        <div className="filtering-main">
            <BodyStyle/>
            <div className="filtering-box">
                <h4>Price</h4>
                <div className="htmlForm-group green">
                    <Price />
                </div>
            </div>
            <div className="filtering-box">
                <h4>Year</h4>
                <div className="htmlForm-group blue">
                    <Year />
                </div>
            </div>
            <div className="filtering-box">
                <h4>Mileage</h4>
                <div className="htmlForm-group blue">
                    <Mileage />
                </div>
            </div>
            <div className="filtering-box">
                <h4>Colors</h4>
                <div className="colors-main">
                    <div className="color-box">
                        <input type="radio" name="color" id="color" />
                        <label htmlFor="color" className="black"></label>
                    </div>
                    <div className="color-box">
                        <input type="radio" name="color" id="color2" />
                        <label htmlFor="color2" className="grey"></label>
                    </div>
                    <div className="color-box">
                        <input type="radio" name="color" id="color3" />
                        <label htmlFor="color3" className="grey-light"></label>
                    </div>
                    <div className="color-box">
                        <input type="radio" name="color" id="color4"/>
                        <label htmlFor="color4" className="white"></label>
                    </div>
                    <div className="color-box">
                        <input type="radio" name="color" id="color5" />
                        <label htmlFor="color5" className="red"></label>
                    </div>
                    <div className="color-box">
                        <input type="radio" name="color" id="color6" />
                        <label htmlFor="color6" className="blue"></label>
                    </div>
                    <div className="color-box">
                        <input type="radio" name="color" id="color7" />
                        <label htmlFor="color7" className="orange"></label>
                    </div>
                </div>
            </div>
            <div className="filtering-box">
                <h4>Transmission</h4>
                <div className="btn btn-secondary btn-transm active">Automatic</div>
                <div className="btn btn-secondary btn-transm">Manual</div>
            </div>
            <div className="filtering-box">
                <div className="btn btn-secondary btn-block">Reset Filters</div>
                <div className="btn btn-secondary btn-block active">Save Search</div>
            </div>
        </div>
    )
}
}
export default Filters;



