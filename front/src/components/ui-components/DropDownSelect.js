/**
*
* DropDownSelect
*
*/

import React from 'react';

class DropDownSelect extends React.Component { // eslint-disable-line react/prefer-stateless-function

  renderSelectOptions = (person) => (
    <option key={person} value={person}>{person}</option>
  )

  render() {
    const { input, label } = this.props;
    return (
      <div>
        {/* <label htmlFor={label}>{label}</label> */}
        <select {...input} className="form-control">
          <option value="">Select {label}</option>
          {this.props.categories.map(this.renderSelectOptions)}
        </select>
      </div>
    );
  }
}

export default DropDownSelect;