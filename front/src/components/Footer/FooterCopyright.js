import React, { Component } from 'react'

const FooterCopyright = () => {
    let year = (new Date()).getFullYear()
    return (
        <div className="copyright-bg">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-12">
                        <p>Copyright &copy; {year} AYG Auto Inc.</p>
                    </div>
                    <div className="col-md-6 col-12">
                        <ul className="menu float-right">
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FooterCopyright;