
import Homepage from "../../containers/Homepage";
import Products from "../../containers/ProductListing";
import Product_Details from "../../containers/ProductDetails";
// import React, { Component } from 'react'
// import seoApi from "../../api/seoApi"
// class Seo extends Component {
//     state = { seo_car: [], seo: [{}] };
//     componentDidMount = () => {
//         this.fetch();
//         let id = 80;
//         seoApi.getCarSeo(id).then(response => {
//             this.setState({ seo_car: response });
//             console.log("response", response);
//         });
//     }
//     fetch = () => {
//         seoApi.getSeo().then(response => {            
//             this.setState({ seo: response });
//         });

//     }
//   render() {
//     //console.log("Seo", this.state.seo[0]['route']);
//     return (
//       <div>
        
//       </div>
//     )
//   }
// }

const appRoutes = [
    
    
    { path: "/", exact: true, name: "Home", component: Homepage, title: 'Looking for Used Cars for Sale in Florida - Aygauto', description: 'Looking for used car in perfect condition and right price? Call AYG for the best deals.', keyword:'used cars Florida, used cars for sale in Florida, used car Florida sale'},
    { path: "/cars", exact: true, name: "cars", component: Products, title: 'Wondering how to get used cars in Florida? - Aygauto', description: ' Search our inventory of certified used cars for sale in Florida at Enterprise Car Sales. You will get a large variety of quality cars in different brands, models, body shape & color with price just according to your budget scales.', keyword: 'Buying the used car in Florida, used cars for sale in Florida' },
    { path: "/car-details/:id", exact: true, name: "car-details", component: Product_Details, title: 'Are you looking for used cars for sale in Florida? - Aygauto', description: "Now buying the used cars in Florida is as easy & economical as you could wish. We have got a huge variety of top-class brands including Volkswagens, Honda, Nissan, Hyundai , Toyota, and many more.", keyword: "Buying the used car in Florida, used cars for sale in Florida" }, 

   
];

export default appRoutes;