import React, { Component } from 'react'

const Whyform = () => (
	<section className="whyfrom-bg">
		<div className="container">
			<div className="row">
				<div className="col-12">
					<h3 className="text-center">Why buy from us?</h3>
				</div>			
				<div className="col-md-4 col-12">
					<div className="whyfrom-box">
						<i className="focused-icon"></i>
						<h5>We're Focused on You</h5>
						<p>We always work with our customers to get them what they need for the right price.</p>
					</div>
				</div>
				<div className="col-md-4 col-12">
					<div className="whyfrom-box">
						<i className="haggle-icon"></i>
						<h5>No Need To Haggle</h5>
						<p>We will always price the car at the lowest in the region to ensure you get the best deal.</p>
					</div>
				</div>
				<div className="col-md-4 col-12">
					<div className="whyfrom-box">
						<i className="confidence-icon"></i>
						<h5>Buy With Confidence</h5>
						<p>We only sell cars with good quality mechanics to ensure your drive is safe and enduring.</p>
					</div>
				</div>
			</div>
		</div>
	</section>    
)

export default Whyform;