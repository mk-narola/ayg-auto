import React, { Component } from 'react'
import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import { Link } from "react-router-dom";
import { Field, reduxForm, initialize,FieldArray } from "redux-form";
import DropDownSelect from "../../components/ui-components/DropDownSelect";
import _ from 'lodash';
import carApi from "../../api/carApi";
import {modelRequest,carCategoriesHomeRequest} from "../../actions/carsActions"

export const formFields = ['select_year','car_model','price_car','car_categories','select_year','select_car_type','car_brand','car_model']

class Findcar extends Component {
	handleOptionChange(changeEvent) {
		//console.log(changeEvent.target.value);
		this.props.carCategoriesHomeRequest(changeEvent.target.value);
		//this.setState({selectedOption:changeEvent.target.value });
	}
	change = (event) => {
		//alert(event.target.value);
		this.props.modelRequest(event.target.value);
	}
	renderSelectOptions = (value) => (
		<option key={value} value={value}>{value}</option>
	  )
	render() {
		const {handleSubmit,fields} = this.props
		//console.log("Model ",this.props.models);
	return (
		
<section className="findcar-bg">
		<div className="container">
			<div className="row">
				<div className="col">
					<div className="findcar-box">
					<form id="findCar" method="post" onSubmit={handleSubmit(this.props.onSubmit)}>
						<div className="row">
							<div className="col-12 col-md-4">
								<div className="form-group select">
									<select id="select" className="form-control" name="select_car_type">
										<option value="">Select Car</option>
										<option value="new-cars">New Cars</option>
										<option value="used-cars">Used Cars</option>
										<option value="new-and-used-cars">New and Used Cars</option>
									</select>
								</div>
								<div className="form-group select">
								<select id="select" name="car_brand" className="form-control" onChange={event => this.change(event)}>
										<option value="">Select Brand</option>
										{ _.isArray(this.props.initialValuesBrand) ? this.props.initialValuesBrand.map(this.renderSelectOptions) : [].map(this.renderSelectOptions)}
									</select>
								</div>
								<div className="form-group select">
									<select id="select" name="car_model" className="form-control">
										<option value="">Select Model</option>
										{ _.isArray(this.props.models) ? this.props.models.map(this.renderSelectOptions) : [].map(this.renderSelectOptions)}
									</select>
								</div>
								<div className="form-group select">
									<select id="select" name="select_year" className="form-control">
										<option value="">Select Year</option>
										<option value="2018">2018</option>
										<option value="2017">2017</option>
										<option value="2016">2016</option>
										<option value="2015">2015</option>
										<option value="2014">2014</option>
										<option value="2013">2013</option>
										<option value="2012">2012</option>
										<option value="2011">2011</option>
										<option value="2010">2010</option>
										<option value="2009">2009</option>
										<option value="2008">2008</option>
										<option value="2007">2007</option>
										
									</select>
								</div>
							</div>
							<div className="col-12 col-md-5">
								<div className="car-main">
									<div className="car-box">
										<input type="radio" value="economical" name="car_categories" id="cars" onChange={this.handleOptionChange.bind(this)}/>
										<label>
											<i className="car-icon"></i>
											<span>Economical</span>
										</label>
									</div>
									<div className="car-box">
										<input type="radio" value="sedan" name="car_categories" id="cars2" onChange={this.handleOptionChange.bind(this)}/>
										<label>
											<i className="car-icon sedan"></i>
											<span>Sedan</span>
										</label>
									</div>
									<div className="car-box">
										<input type="radio"  value="coupe"  name="car_categories" id="cars3" onChange={this.handleOptionChange.bind(this)}/>
										<label>
											<i className="car-icon coupe"></i>
											<span>Coupe</span>
										</label>
									</div>
									<div className="car-box">
										<input type="radio" value="hatchback" name="car_categories" id="cars4" onChange={this.handleOptionChange.bind(this)}/>
										<label>
											<i className="car-icon hatch-back"></i>
											<span>Hatchback</span>
										</label>
									</div>
									<div className="car-box">
										<input type="radio" value="convertible" name="car_categories" id="cars5" onChange={this.handleOptionChange.bind(this)}/>
										<label>
											<i className="car-icon convertible"></i>
											<span>Convertible</span>
										</label>
									</div>
									<div className="car-box">
										<input type="radio" value="wagon" name="car_categories" id="cars6" onChange={this.handleOptionChange.bind(this)}/>
										<label>
											<i className="car-icon wagon"></i>
											<span>Wagon</span>
										</label>
									</div>
									<div className="car-box">
										<input type="radio" value="suv" name="car_categories" id="cars7" onChange={this.handleOptionChange.bind(this)}/>
										<label>
											<i className="car-icon suv"></i>
											<span>SUV</span>
										</label>
									</div>
									<div className="car-box">
										<input type="radio" value="mini-van" name="car_categories" id="cars8" onChange={this.handleOptionChange.bind(this)}/>
										<label>
											<i className="car-icon mini-van"></i>
											<span>Mini-Van</span>
										</label>
									</div>
									<div className="car-box">
										<input type="radio" value="truck" name="car_categories" id="cars9" onChange={this.handleOptionChange.bind(this)}/>
										<label>
											<i className="car-icon truck"></i>
											<span>Truck</span>
										</label>
									</div>
								</div>
							</div>
							<div className="col-12 col-md-3">
								<div className="p-limit-radio">
									<input type="radio" value="5000" name="price_car" id="p-limit" />
									<lable className="btn btn-success">Less Than $5k</lable>
								</div>
								<div className="p-limit-radio">
									<input type="radio" value="15000_25000" name="price_car" id="p-limit" />
									<lable className="btn btn-success">$15 - $25k</lable>
								</div>
								<div className="p-limit-radio">
									<input type="radio" value="25000_35000" name="price_car" id="p-limit" />
									<lable className="btn btn-success">$25 - $35k</lable>
								</div>
								<div className="p-limit-radio">
									<input type="radio" value="35000" name="price_car" id="p-limit" />
									<lable className="btn btn-success">More Than $35k</lable>
								</div>								
							</div>
							<div className="col-12 text-center">
								<button  type="reset" className="btn btn-link float-left"><i className="fa fa-times"></i><span>Clear Filters</span></button>
								<button type="submit" name="find_car" className="btn btn-primary">Find My Next Car</button>
								<Link to="/cars" className="btn btn-link float-right"><span>Advanced Search</span><i className="fa fa-angle-right"></i></Link>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</section> 
	);
	}
}
Findcar = reduxForm({
	form: 'findCar',
	fields: formFields,
	enableReinitialize: true,	
})(Findcar)

function mapStateToProps({car}) {
	return {...car}
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(modelRequest,carCategoriesHomeRequest, dispatch)
    }
}
export default connect(mapStateToProps, {modelRequest,carCategoriesHomeRequest})(Findcar)