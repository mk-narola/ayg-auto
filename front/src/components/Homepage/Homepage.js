import React, { Component } from 'react'

import Banner from './Banner'
import Findcar from './Findcar'
import Whyform from './Whyform'
import Financing from './Financing'
import Couch from './Couch'
import Feedback from './Feedback'

const Homepage = () => (
    <div>
        <Banner />
        <Findcar />
        <Whyform />
        <Financing />
        <Couch />
        <Feedback />
    </div>
)

export default Homepage;