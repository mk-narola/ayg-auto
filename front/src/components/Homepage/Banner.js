import React, { Component } from 'react'

const Banner = () => (
	<section className="banner-bg">
		<div className="container">
			<div className="row">
				<div className="col text-center">
					<h2>Welcome to Tampa Bay's Best Dealership</h2>
				</div>
			</div>
		</div>
	</section>    
)
export default Banner;