import React, { Component } from 'react'

const Financing = () => (
    <section className="financing-bg">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <h3 className="text-center">Applying for financing is easy.</h3>
                </div>
                <div className="col-md-4 col-12">
                    <div className="financing-box">
                        <i className="findcar-icon"></i>
                        <h5>Find Your Car</h5>
                        <p>Finding your car is easy on our website.</p>
                    </div>
                </div>
                <div className="col-md-4 col-12">
                    <div className="financing-box">
                        <i className="option-icon"></i>
                        <h5>See Your Options</h5>
                        <p>Select from a wide arrange of inventory to chose from.</p>
                    </div>
                </div>
                <div className="col-md-4 col-12">
                    <div className="financing-box">
                        <i className="approved-icon"></i>
                        <h5>Get Approved</h5>
                        <p>Apply for Financing with us or with our financing partners.</p>
                    </div>
                </div>
                <div className="col-12 text-center">
                    <a href="#" className="btn btn-info">Apply</a>
                </div>
            </div>
        </div>
    </section>
)

export default Financing;