import React, { Component } from "react";
import { Link } from "react-router-dom";

const Couch = () => (
  <section className="couch-bg">
    <div className="container">
      <div className="row">
        <div className="col-12">
          <h3 className="text-center">
            Buy your next car from your couch!
            <span>
              Our concierge service is here to make car buying easier than ever.
            </span>
          </h3>
          <div className="step-box">
            <h4>Selecting a car is easy.</h4>
          </div>
          <div className="step-box">
            <h4>Schedule a test drive with our team.</h4>
          </div>
          <div className="step-box">
            <h4>Apply for financing.</h4>
          </div>
        </div>
        <div className="col-12 text-center">
          <Link to="/cars" className="btn btn-primary">
            Find My Car
          </Link>
        </div>
      </div>
    </div>
  </section>
);

export default Couch;
