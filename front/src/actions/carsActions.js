import * as types from './actionTypes';
import carApi from '../api/carApi';

export function brandRequestInit() {
    return {type: types.CAR_BRAND_REQUEST}
}

export function brandRequestSuccess(payload) {
    return {type: types.CAR_BRAND_SUCCESS, payload}
}

export function brandRequestFail() {
    return {type: types.CAR_BRAND_FAILER}
}
export function brandRequest() {
    return function (dispatch) {
        dispatch(brandRequestInit())
        return carApi.getBrands()
            .then(response => {
                dispatch(brandRequestSuccess(response))
            })
            .catch(error => {
                throw error
            })
    }
}

export function modelRequestInit() {
    return {type: types.CAR_MODEL_REQUEST}
}
export function modelRequestSuccess(payload) {
    return {type: types.CAR_MODEL_SUCCESS, payload}
}
export function modelRequestFail() {
    return {type: types.CAR_MODEL_FAILER}
}
export function modelRequest(brand) {
    return function (dispatch){
        dispatch(modelRequestInit())
        return carApi.getBrandModels(brand).then(response => {
            dispatch(modelRequestSuccess(response))
        })
        .catch(error => {
            throw error
        })
    }
}

export function carsRequestInit() {
    return {type: types.CARS_REQUEST}
}
export function carsRequestSuccess(payload) {
    return {type: types.CARS_SUCCESS, payload}
}
export function carsRequestFail() {
    return {type: types.CARS_FAILER}
}
export function carsRequest() {
    return function (dispatch){
        dispatch(carsRequestInit())
        return carApi.getCarsAll().then(response => {
            dispatch(carsRequestSuccess(response))
        })
        .catch(error => {
            throw error
        })
    }
}

export function carDetailsRequestInit() {
    return {type: types.CAR_DETAILS_REQUEST}
}
export function carDetailstSuccess(payload) {
    return {type: types.CAR_DETAILS_SUCCESS, payload}
}
export function carDetailsRequestFail() {
    return {type: types.CAR_DETAILS_FAILER}
}
export function carDetailsRequest(id) {
    return function (dispatch){
        dispatch(carDetailsRequestInit())
        return carApi.getCarDetails(id).then(response => {
            dispatch(carDetailstSuccess(response))
        })
        .catch(error => {
            throw error
        })
    }
}

export function carFliterHomeRequestInit(data) {
    var selectedData = []
    selectedData.select_car_type = data.get('select_car_type');
    selectedData.car_brand = data.get('car_brand');
    selectedData.car_model = data.get('car_model');
    selectedData.select_year = data.get('select_year');
    selectedData.car_categories = data.get('car_categories');
    selectedData.price_car = data.get('price_car');
    //console.log('selectedData',selectedData);
    return {type: types.CAR_FLITER_HOME_REQUEST,selectedData}
}
export function carFliterHomeSuccess(payload) {
    return {type: types.CAR_FLITER_HOME_SUCCESS, payload}
}
export function carFliterHomeRequestFail() {
    return {type: types.CAR_FLITER_HOME_FAILER}
}
export function carFliterHomeRequest(data,callback) {
    return function (dispatch){
        dispatch(carFliterHomeRequestInit(data))
        return carApi.getCarFliterHome(data).then(response => {
            dispatch(carFliterHomeSuccess(response))
            callback();
        })
        .catch(error => {
            throw error
        })
    }
}

export function carCategoriesHomeRequestInit() {
    return {type: types.CAR_CATEGORIES_SELECT_REQUEST}
}
export function carCategoriesHomeSuccess(payload) {
    return {type: types.CAR_CATEGORIES_SELECT_SUCCESS, payload}
}
export function carCategoriesHomeRequest(data) {
    return function (dispatch){
        dispatch(carCategoriesHomeRequestInit())
        return dispatch(carCategoriesHomeSuccess(data))
    }
}


export function carFliterCarsRequestInit(key,value) {
    
    return { type: types.CAR_FILTER_CARS_REQUEST}
}
export function carFliterCarsSuccess(payload) {
    return { type: types.CAR_FILTER_CARS_SUCCESS, payload }
}
export function carFliterCarsRequestFail() {
    return { type: types.CAR_FILTER_CARS_FAILER }
}
export function carFliterCarsRequest(key,value) {
    return function (dispatch) {
        dispatch(carFliterCarsRequestInit(key,value))
        return carApi.getCarFilter(key,value).then(response => {
            dispatch(carFliterCarsSuccess(response));
            
        })
            .catch(error => {
                throw error
            })
    }
}

