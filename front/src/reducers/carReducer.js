import * as types from '../actions/actionTypes'
import intialState from './intialState'

export default function (state = intialState.brand, action) {
    switch (action.type) {
        case types.CAR_BRAND_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.CAR_BRAND_SUCCESS:
            return {
                ...state,
                isFetching: false,
                brand: action.payload
            }
        case types.CAR_BRAND_FAILER:
            return {
                ...state,
                isFetching: false,
                brand: false
            }
        case types.CAR_MODEL_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.CAR_MODEL_SUCCESS:
            return {
                ...state,
                isFetching: false,
                models: action.payload
            }
        case types.CAR_MODEL_FAILER:
            return {
                ...state,
                isFetching: false,
                models: false
            }
        case types.CARS_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.CARS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                carsData: action.payload
            }
        case types.CARS_FAILER:
            return {
                ...state,
                isFetching: false,
                carsData: false
            }
        case types.CAR_DETAILS_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.CAR_DETAILS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                carDetails: action.payload
            }
        case types.CAR_DETAILS_FAILER:
            return {
                ...state,
                isFetching: false,
                carDetails: false
            }

        case types.CAR_CATEGORIES_SELECT_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.CAR_CATEGORIES_SELECT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                categories: action.payload
            }
        case types.CAR_FLITER_HOME_REQUEST:
            return {
                ...state,
                isFetching: true,
                selectedData: action.selectedData
                
            }
        case types.CAR_FLITER_HOME_SUCCESS:
            return {
                ...state,
                isFetching: false,
                carsFliterData: action.payload
            }
        case types.CAR_FLITER_HOME_FAILER:
            return {
                ...state,
                isFetching: false,
                carsFliterData: false
            }

        case types.CAR_FILTER_CARS_REQUEST:
            return {
                ...state,
                isFetching: true,
                

            }
        case types.CAR_FILTER_CARS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                filterData: action.payload
            }
        case types.CAR_FILTER_CARS_FAILER:
            return {
                ...state,
                isFetching: false,
                filterData: false
            }
        default:
            return state
    }
}