import { combineReducers } from "redux";
import carReducer from "./carReducer";
import { reducer as formReducer } from "redux-form";
const rootReducers = combineReducers({
    car: carReducer,
    form:formReducer
   
});

export default rootReducers;