import React, { Component } from 'react'
import {connect} from "react-redux"
import {bindActionCreators} from "redux"
//import carApi from "../api/carApi"brandRequestInit,
import {brandRequest,carFliterHomeRequest} from "../actions/carsActions"

import Banner from '../components/Homepage/Banner'
import Findcar from '../components/Homepage/Findcar'
import Whyform from '../components/Homepage/Whyform'
import Financing from '../components/Homepage/Financing'
import Couch from '../components/Homepage/Couch'
import Feedback from '../components/Homepage/Feedback'

class Homepage extends Component {  
    onSubmit = (values) => {
        var formData = document.getElementById('findCar');
        let data = new FormData(formData);
        // carApi.getCarFliterHome(data);
        this.props.carFliterHomeRequest(data,()=>{
            this.props.history.push('/cars');
        });
    }  
    state = {brand:[]};
    componentDidMount() {
        this.props.brandRequest();
    }
    render() {
        return (
            <div className="wap">
            <Banner />
            <Findcar 
            initialValuesBrand = {this.props.brand}
            onSubmit={this.onSubmit}
            />
            <Whyform />
            <Financing />
            <Couch />
            <Feedback />
            </div>
        )
    }
}
function mapStateToProps({car}) {
    //console.log(car);
    return {...car}
}


export default connect(mapStateToProps, {brandRequest,carFliterHomeRequest})(Homepage)